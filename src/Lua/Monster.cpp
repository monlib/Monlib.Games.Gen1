#include "Monlib/Games/Gen1/Lua/Monster.h"

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Interpret/Encodings.h"
#include "Monlib/Games/Gen1/Interpret/Monster.h"
#include "Monlib/Games/Gen1/Lua/BinaryFormatLists.h"
#include "Monlib/Games/Gen1/Lua/BinaryFormats.h"
#include "Monlib/Games/Gen1/Range.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {
        constexpr std::array EditablePropertyNames{
            "originalTrainerId",
        };

        struct MutableMonster {
            Bytes::MutableMonster monster;
            shared_dynarray<std::byte> nickname;
            shared_dynarray<std::byte> otName;

            Monster AsImmutable(std::shared_ptr<Mappers> mappers) const {
                return Monster{monster, nickname, otName, mappers};
            }
        };

        template <class T>
        std::shared_ptr<std::remove_cv_t<T>> CopySharedValue(std::shared_ptr<T> s) {
            if (s != nullptr) {
                return std::make_shared<std::remove_cv_t<T>>(*s);
            } else {
                return nullptr;
            }
        }

        MutableMonster MutableCopy(const Bytes::Monster& m,
            shared_dynarray<const std::byte> n,
            shared_dynarray<const std::byte> o) {
            return {m.Copy(), copy_shared_dynarray(n), copy_shared_dynarray(o)};
        }
    }

    Monster::Monster(Bytes::Monster monster,
        shared_dynarray<const std::byte> nickname,
        shared_dynarray<const std::byte> otName,
        std::shared_ptr<Mappers> mappers) :
        _monster{monster},
        _nickname{nickname},
        _otName{otName},
        _mappers{mappers} {
        Expects(mappers != nullptr);
    }

    int Monster::GetSpecies() const {
        return _mappers->species(_monster.GetSpecies());
    }

    int Monster::GetExperience() const { return Interpret::GetMonsterExp(_monster); }

    int Monster::GetLevel() const { return Interpret::GetMonsterLevel(_monster); }

    std::string Monster::GetNickname() const {
        return Interpret::Utf8FromEnglishBytes(_nickname).value_or("");
    }

    std::string Monster::GetOtName() const {
        return Interpret::Utf8FromEnglishBytes(_otName).value_or("");
    }

    int Monster::GetOtId() const { return Interpret::GetMonsterOtId(_monster); }

    sol::object Monster::EditableProperties(sol::this_state s) const {
        return sol::make_object(s, sol::as_table(EditablePropertyNames));
    }

    sol::variadic_results Monster::ModifiedWith(sol::this_state s,
        sol::table changes,
        sol::optional<bool> ignoreUnknown) const {
        if (!ignoreUnknown.value_or(false)) {
            bool containsInvalidKey = false;
            changes.for_each([&containsInvalidKey](sol::object key, sol::object) {
                containsInvalidKey = containsInvalidKey || !key.is<std::string>()
                    || (!std::find(EditablePropertyNames.begin(),
                           EditablePropertyNames.end(),
                           key.as<std::string>()));
            });
            if (containsInvalidKey) {
                return {sol::nil, sol::object(s, sol::in_place, "Invalid key!")};
            }
        }
        MutableMonster copy = MutableCopy(_monster, _nickname, _otName);
        if (changes["originalTrainerId"] != sol::nil) {
            sol::optional<int> otId = changes["originalTrainerId"];
            Interpret::SetMonsterOtId(copy.monster, *otId);
        }
        Monster result = copy.AsImmutable(_mappers);
        return {sol::object(s, sol::in_place, result)};
    }

    sol::table Monster::GetBinaryFormats(sol::this_state s) const {
        return GetFormatList().GetTags(s);
    }

    sol::variadic_results Monster::SerializeTo(
        sol::this_state s, std::string format) const {
        try {
            auto result = GetFormatList().Serialize(BinaryFormatTag{format}, *this);
            return {sol::object(s, sol::in_place, result)};
        } catch (const std::exception& e) {
            return {sol::nil, sol::object(s, sol::in_place, e.what())};
        }
    }

    bool Monster::IsTeam() const { return Interpret::IsTeamMonster(_monster); }
    bool Monster::IsBox() const { return Interpret::IsBoxMonster(_monster); }
    MonsterStorageType Monster::GetStorageType() const {
        if (IsTeam()) {
            return MonsterStorageType::Team;
        } else if (IsBox()) {
            return MonsterStorageType::Box;
        } else {
            Expects(false);
        }
    }

    int Monster::SumOfBytes() const {
        return accumulate_bytes(_monster.GetData(), 0)
            + accumulate_bytes(_nickname, 0) + accumulate_bytes(_otName, 0);
    }

    const BinaryFormatList& Monster::GetFormatList() const {
        return BinaryFormats(GetStorageType());
    }

    Bytes::Monster Monster::GetMonsterBytes() const { return _monster; }
    shared_dynarray<const std::byte> Monster::GetNicknameBytes() const {
        return _nickname;
    }
    shared_dynarray<const std::byte> Monster::GetOtNameBytes() const {
        return _otName;
    }

    sol::usertype<Monster> MakeMonsterUsertype() {
        // clang-format off
        return {
            "species", sol::property(&Monster::GetSpecies),
            "experience", sol::property(&Monster::GetExperience),
            "level", sol::property(&Monster::GetLevel),
            "nickname", sol::property(&Monster::GetNickname),
            "originalTrainerName", sol::property(&Monster::GetOtName),
            "originalTrainerId", sol::property(&Monster::GetOtId),
            "editableProperties", sol::property(&Monster::EditableProperties),
            "modifiedWith", &Monster::ModifiedWith,
            "binaryFormats", sol::property(&Monster::GetBinaryFormats),
            "serializeTo", &Monster::SerializeTo,
        };
        // clang-format on
    }
}
