#include "Monlib/Games/Gen1/Lua/LuaOpen.h"

#include "Monlib/Games/Gen1/Lua/Box.h"
#include "Monlib/Games/Gen1/Lua/Save.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"
#include "Monlib/Games/Gen1/Lua/StorageSystem.h"

namespace Monlib::Games::Gen1 {
    namespace {
        template <class T>
        void AddUsertype(sol::table t, std::string name, sol::usertype<T> u) {
            t.set_usertype(name, u);
        }

        sol::table OpenLua(sol::this_state L) {
            sol::state_view lua{L};
            sol::table module = lua.create_table();
            AddUsertype(module, "Save", Lua::MakeSaveUsertype());
            AddUsertype(module, "StorageSystem", Lua::MakeStorageSystemUsertype());
            AddUsertype(module, "Box", Lua::MakeBoxUsertype());
            AddUsertype(module, "Monster", Lua::MakeMonsterUsertype());
            return module;
        }
    }
}

extern "C" int luaopen_monlib_games_gen1(lua_State* L) noexcept {
    // Aborts on exceptions because I don't know how to handle
    // exceptions which may have corrupted the Lua state.
    return sol::stack::call_lua(L, 1, Monlib::Games::Gen1::OpenLua);
}
