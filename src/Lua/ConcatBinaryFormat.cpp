#include "Monlib/Games/Gen1/Lua/ConcatBinaryFormat.h"

#include "Monlib/Games/Gen1/Bytes/VersionDifferences.h"
#include "Monlib/Games/Gen1/Lua/Monster.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {}

    ConcatBinaryFormat::ConcatBinaryFormat(MonsterStorageType storageType) :
        _storageType{storageType} {}

    int ConcatBinaryFormat::OutputSize() const {
        auto string = Bytes::GetNicknameLength(Bytes::Region::Usa);
        auto monster = MonsterSize(_storageType);
        return monster + 2 * string;
    }

    LuaBinaryData ConcatBinaryFormat::Serialize(const Monster& mon) const {
        auto nickname = mon.GetNicknameBytes();
        auto otName = mon.GetOtNameBytes();
        auto monster = mon.GetMonsterBytes();
        Expects(mon.GetStorageType() == _storageType);
        std::string result{};
        result.reserve(OutputSize());
        result.append(reinterpret_cast<const char*>(monster.GetData().data()),
            monster.GetData().size());
        result.append(reinterpret_cast<const char*>(otName.data()), otName.size());
        result.append(
            reinterpret_cast<const char*>(nickname.data()), nickname.size());
        Ensures(gsl::narrow<int>(result.size()) == OutputSize());
        return result;
    }

    std::shared_ptr<const Monster> ConcatBinaryFormat::Deserialize(
        const LuaBinaryData& data, std::shared_ptr<Mappers> mappers) const {
        Expects(gsl::narrow<int>(data.size()) == OutputSize());
        auto block = make_shared_dynarray<std::byte>(OutputSize());
        std::transform(begin(data), end(data), begin(block), [](char c) {
            return std::byte(c);
        });

        auto monSize = _storageType == MonsterStorageType::Team
            ? Bytes::TeamMonsterSize
            : Bytes::BoxedMonsterSize;
        Bytes::Monster mon{slice_dynarray(block, 1, monSize)};
        auto stringSize = Bytes::GetNicknameLength(Bytes::Region::Usa);
        auto otName = slice_dynarray(block, 1 + monSize, stringSize);
        auto nickname = slice_dynarray(block, 1 + monSize + stringSize, stringSize);
        return std::make_shared<Monster>(mon, nickname, otName, mappers);
    }
}
