#include "Monlib/Games/Gen1/Lua/Save.h"

#include "Monlib/Games/Gen1/ArraySpeciesMapper.h"
#include "Monlib/Games/Gen1/Interpret/Save.h"

namespace Monlib::Games::Gen1::Lua {
    Save::Save(const Bytes::Save& save, std::shared_ptr<Mappers> mappers) :
        _storageSystem{std::make_shared<const StorageSystem>(save, mappers)},
        _bank0{copy_shared_dynarray(save.GetBank0())},
        _bank1PreTrash{copy_shared_dynarray(save.GetBank1PreTrash())},
        _trainerName{copy_shared_dynarray(save.GetTrainerNameBytes())},
        _spriteData{copy_shared_dynarray(save.GetSpriteData())},
        _mainDataWoDaycare{copy_shared_dynarray(save.GetMainDataWoDaycare())},
        _tilesetType{save.GetTilesetByte()},
        _mainDataChecksum{save.GetChecksumByte()},
        _bank1PostTrash{copy_shared_dynarray(save.GetBank1PostTrash())},
        _boxBanksTrash{copy_shared_dynarray(save.GetBoxBankTrash(0)),
            copy_shared_dynarray(save.GetBoxBankTrash(1))},
        _region{save.GetRegion()},
        _mappers{std::move(mappers)} {
        Expects(_mappers != nullptr);
        Ensures(_storageSystem && _bank0 != nullptr && _bank1PreTrash != nullptr
            && _trainerName != nullptr && _spriteData != nullptr
            && _mainDataWoDaycare != nullptr && _bank1PostTrash != nullptr
            && _boxBanksTrash[0] != nullptr && _boxBanksTrash[1] != nullptr
            && _mappers != nullptr);
    }

    Save::Save(decltype(_storageSystem) storageSystem,
        decltype(_bank0) bank0,
        decltype(_bank1PreTrash) bank1PreTrash,
        decltype(_trainerName) trainerName,
        decltype(_spriteData) spriteData,
        decltype(_mainDataWoDaycare) mainDataWoDaycare,
        decltype(_tilesetType) tilesetType,
        std::optional<decltype(_mainDataChecksum)> mainDataChecksumOpt,
        decltype(_bank1PostTrash) bank1PostTrash,
        decltype(_boxBanksTrash) boxBanksTrash,
        decltype(_region) region,
        decltype(_mappers) mappers) :
        _storageSystem{std::move(storageSystem)},
        _bank0{std::move(bank0)},
        _bank1PreTrash{std::move(bank1PreTrash)},
        _trainerName{std::move(trainerName)},
        _spriteData{std::move(spriteData)},
        _mainDataWoDaycare{std::move(mainDataWoDaycare)},
        _tilesetType{std::move(tilesetType)},
        _bank1PostTrash{std::move(bank1PostTrash)},
        _boxBanksTrash{std::move(boxBanksTrash)},
        _region{std::move(region)},
        _mappers{std::move(mappers)} {
        Ensures(_storageSystem && _bank0 != nullptr && _bank1PreTrash != nullptr
            && _trainerName != nullptr && _spriteData != nullptr
            && _mainDataWoDaycare != nullptr && _bank1PostTrash != nullptr
            && _boxBanksTrash[0] != nullptr && _boxBanksTrash[1] != nullptr
            && _mappers != nullptr);
        _mainDataChecksum =
            mainDataChecksumOpt.value_or(CalculateMainDataChecksum());
    }

    std::byte Save::CalculateMainDataChecksum() const {
        using std::begin, std::end;
        // no std::accumulate because std::byte != uint8_t
        auto region = ChecksummedRegion();
        uint8_t checksum = 255;
        for (auto b : region) {
            checksum -= std::to_integer<uint8_t>(b);
        }
        return std::byte(checksum);
    }

    dynarray<const std::byte> Save::ChecksummedRegion() const {
        auto copy = [](auto&& c, auto&& o) {
            using std::begin, std::end;
            return std::copy(begin(c), end(c), o);
        };
        auto push_back = [](auto&& v, auto o) {
            *o = v;
            return ++o;
        };
        auto daycare = _storageSystem->GetDaycareBytes();
        auto team = _storageSystem->GetTeamBytes();
        auto activeBox = _storageSystem->GetActiveBoxBytes();
        auto result = make_dynarray<std::byte>(_trainerName.size()
            + _mainDataWoDaycare.size() + daycare.size() + _spriteData.size()
            + team.size() + activeBox.size() + 1);
        auto curr = result.begin();
        curr = copy(_trainerName, curr);
        curr = copy(_mainDataWoDaycare, curr);
        result[GetCurrentBoxOffset()] = _storageSystem->GetCurrentBoxByte();
        curr = copy(daycare, curr);
        curr = copy(_spriteData, curr);
        curr = copy(team, curr);
        curr = copy(activeBox, curr);
        curr = push_back(_tilesetType, curr);
        Expects(curr == end(result));
        return std::move(result);
    }

    std::shared_ptr<const StorageSystem> Save::GetStorageSystem() const {
        return _storageSystem;
    }

    sol::object MakeSave(
        std::string filename, sol::table speciesMapping, sol::this_state s) {
        auto saveOpt = Bytes::LoadSave(filename);
        if (saveOpt) {
            sol::state_view lua{s};
            auto mappers = std::make_shared<Mappers>();
            ArraySpeciesMapper speciesMapper{0x00};
            for (std::size_t i = 1; i <= speciesMapping.size(); ++i) {
                speciesMapper.RegisterSpecies(std::byte(i - 1), speciesMapping[i]);
            }
            mappers->species = speciesMapper;
            return sol::object{lua, sol::in_place, Save{*saveOpt, mappers}};
        } else {
            return sol::nil;
        }
    }

    std::string Save::SerializeToString() const {
        auto copy = [](auto&& c, auto&& o) {
            using std::begin, std::end;
            return std::copy(begin(c), end(c), o);
        };
        auto push_back = [](auto&& v, auto o) {
            *o = v;
            return ++o;
        };
        std::vector<std::byte> result{};
        result.reserve(0x8000);
        auto curr = std::back_inserter(result);
        curr = copy(_bank0, curr);
        Expects(result.size() == 0x2000);
        curr = copy(_bank1PreTrash, curr);
        curr = copy(ChecksummedRegion(), curr);
        curr = push_back(_mainDataChecksum, curr);
        curr = copy(_bank1PostTrash, curr);
        Expects(result.size() == 0x4000);
        for (int bank = 0; bank < 2; ++bank) {
            auto bankData = _storageSystem->GetBoxBankBytes(bank);
            for (auto& bytes : bankData) {
                curr = copy(bytes, curr);
            }
            curr = copy(_boxBanksTrash[bank], curr);
        }
        return {reinterpret_cast<const char*>(result.data()), result.size()};
    }

    void Save::SerializeToFile(sol::table file) const {
        sol::optional<sol::protected_function> writeOpt = file["write"];
        if (!writeOpt) {
            throw std::runtime_error("Parameter is not a file!");
        }
        sol::protected_function write = *writeOpt;
        auto result = write(file, SerializeToString());
        if (!result.valid() || result.get_type() == sol::type::nil) {
            throw std::runtime_error("Failed to write save");
        }
    }

    int Save::GetCurrentBoxOffset() const {
        return Bytes::GetCurrentBoxOffset(_region);
    }

    sol::variadic_results Save::WithMonsterStorage(
        sol::object storageObj, sol::this_state lua) const {
        if (!storageObj.is<StorageSystem>()) {
            return LuaError(
                lua, "Paremeter is not a Monlib::Games::Gen1::Lua::StorageSystem");
        }
        // Probably unsafe
        auto storage = storageObj.as<std::shared_ptr<const StorageSystem>>();
        return {sol::object{lua,
            sol::in_place,
            std::make_shared<const Save>(storage,
                _bank0,
                _bank1PreTrash,
                _trainerName,
                _spriteData,
                _mainDataWoDaycare,
                _tilesetType,
                std::nullopt,
                _bank1PostTrash,
                _boxBanksTrash,
                _region,
                _mappers)}};
    }

    sol::usertype<Save> MakeSaveUsertype() {
        // clang-format off
        return {
            "new", sol::factories(&MakeSave),
            "monsterStorage", sol::property(&Save::GetStorageSystem),
            "serializeToString", &Save::SerializeToString,
            "serializeToFile", &Save::SerializeToFile,
            "withMonsterStorage", &Save::WithMonsterStorage,
        };
        // clang-format on
    }
}
