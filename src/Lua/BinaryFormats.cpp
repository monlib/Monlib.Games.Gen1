#include "Monlib/Games/Gen1/Lua/BinaryFormats.h"

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Lua/Monster.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {
        template <std::size_t N>
        std::string ByteArrToString(const std::array<std::byte, N>& a) {
            return std::string{reinterpret_cast<const char*>(a.data()), a.size()};
        }

        std::array<std::byte, 16> DeserializeBinaryFormatTag(LuaBinaryData d) {
            Expects(d.size() == 16);
            using std::begin, std::end;
            std::array<std::byte, 16> bytes;
            std::transform(
                begin(d), end(d), begin(bytes), [](char c) { return std::byte(c); });
            return bytes;
        }
    }

    BinaryFormatTag::BinaryFormatTag(LuaBinaryData d) :
        _bytes{DeserializeBinaryFormatTag(d)} {}

    BinaryFormatTag::operator LuaBinaryData() const {
        return ByteArrToString(_bytes);
    }

    bool operator<(BinaryFormatTag lhs, BinaryFormatTag rhs) {
        return lhs._bytes < rhs._bytes;
    }

    std::shared_ptr<const Monster> BinaryFormatList::Deserialize(
        const BinaryFormatTag& tag,
        const LuaBinaryData& data,
        std::shared_ptr<Mappers> mappers) const {
        return GetFormat(tag).Deserialize(data, mappers);
    }

    LuaBinaryData BinaryFormatList::Serialize(
        const BinaryFormatTag& tag, const Monster& mon) const {
        return GetFormat(tag).Serialize(mon);
    }

    void BinaryFormatList::Register(
        BinaryFormatTag tag, std::unique_ptr<BinaryFormat> format) {
        _formats.emplace(tag, std::move(format));
    }

    sol::table BinaryFormatList::GetTags(sol::state_view lua) const {
        sol::table results = lua.create_table_with();
        for (const auto& pair : _formats) {
            const auto& tag = pair.first;
            results.add(static_cast<LuaBinaryData>(tag));
        }
        return results;
    }

    bool BinaryFormatList::Contains(const BinaryFormatTag& tag) const {
        return _formats.count(tag) > 0;
    }

    const BinaryFormat& BinaryFormatList::GetFormat(
        const BinaryFormatTag& tag) const {
        const auto* format = _formats.at(tag).get();
        Expects(format != nullptr);
        return *format;
    }
}
