#include "Monlib/Games/Gen1/Lua/StorageSystem.h"

#include <variant>

#include "Monlib/Games/Gen1/Interpret/Save.h"
#include "Monlib/Games/Gen1/MakeVisitor.h"
#include "Monlib/Games/Gen1/Range.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {
        shared_dynarray<const std::shared_ptr<const Box>> MakeInactiveBoxes(
            const Bytes::Save& save, std::shared_ptr<Mappers> mappers) {
            auto boxes = make_shared_dynarray<std::shared_ptr<const Box>>(
                Bytes::GetBoxCount(save.GetRegion()));
            for (int i = 0; i < boxes.size(); ++i) {
                boxes[i] =
                    std::make_shared<const Box>(Interpret::GetInactiveBox(save, i),
                        save.GetBoxChecksumByte(i),
                        mappers);
            }
            return std::move(boxes);
        }

        std::shared_ptr<const Box> MakeBox(
            const Bytes::MonsterList& list, std::shared_ptr<Mappers> mappers) {
            return std::make_shared<const Box>(
                list, std::nullopt, std::move(mappers));
        }

        std::shared_ptr<const Box> MakeBox(
            const Bytes::DaycareBlock& daycare, std::shared_ptr<Mappers> mappers) {
            return std::make_shared<const Box>(daycare, std::move(mappers));
        }
    }

    std::optional<int> StorageSystem::ActiveBoxIndex() const {
        auto b = _currentBox;
        auto boxInitialized = (b & std::byte{0x80}) != std::byte{0};
        auto activeBox = std::to_integer<int>(b & std::byte{0x7F});
        if (boxInitialized && 0 <= activeBox && activeBox < _inactiveBoxes.size()) {
            return activeBox;
        } else {
            return std::nullopt;
        }
    }

    StorageSystem::StorageSystem(
        const Bytes::Save& save, std::shared_ptr<Mappers> mappers) :
        StorageSystem(MakeInactiveBoxes(save, mappers),
            MakeBox(Interpret::GetActiveBox(save), mappers),
            MakeBox(Interpret::GetTeam(save), mappers),
            MakeBox(Interpret::GetDaycare(save), mappers),
            save.GetCurrentBoxIndex(),
            {save.GetBoxBankChecksumByte(0), save.GetBoxBankChecksumByte(1)},
            mappers) {}

    StorageSystem::StorageSystem(decltype(_inactiveBoxes) inactiveBoxes,
        decltype(_activeBox) activeBox,
        decltype(_team) team,
        decltype(_daycare) daycare,
        decltype(_currentBox) currentBox,
        decltype(_allBoxesChecksums) allBoxesChecksums,
        decltype(_mappers) mappers) :
        _inactiveBoxes{std::move(inactiveBoxes)},
        _activeBox{std::move(activeBox)},
        _team{std::move(team)},
        _daycare{std::move(daycare)},
        _currentBox{std::move(currentBox)},
        _allBoxesChecksums{std::move(allBoxesChecksums)},
        _mappers{std::move(mappers)} {
        using std::begin, std::end;
        Expects(std::all_of(
            begin(_inactiveBoxes), end(_inactiveBoxes), [](auto&& p) { return p; }));
        Expects(_activeBox);
        Expects(_team);
        Expects(_daycare);
        Expects(_mappers);
    }

    int StorageSystem::GetSize() const { return GetPcBoxCount() + 2; }

    int StorageSystem::GetPcBoxCount() const {
        return ActiveBoxIndex() ? _inactiveBoxes.size() : 0;
    }

    bool StorageSystem::IsTeam(int i) const { return GetSize() - i == 2; }
    bool StorageSystem::IsDaycare(int i) const { return GetSize() - i == 1; }
    bool StorageSystem::IsActiveBox(int i) const {
        return ActiveBoxIndex() && i == *ActiveBoxIndex();
    }

    std::shared_ptr<const Box> StorageSystem::At(sol::object o) const {
        if (o.is<int>()) {
            return AtIndex(o.as<int>() - 1);
        } else {
            return nullptr;
        }
    }

    std::shared_ptr<const Box> StorageSystem::AtIndex(int i) const {
        if (0 <= i && i < GetSize()) {
            if (IsTeam(i)) {
                return GetTeam();
            } else if (IsDaycare(i)) {
                return GetDaycare();
            } else {
                return GetBox(i);
            }
        } else {
            return nullptr;
        }
    }

    std::shared_ptr<const Box> StorageSystem::GetTeam() const { return _team; }

    std::shared_ptr<const Box> StorageSystem::GetDaycare() const { return _daycare; }

    std::shared_ptr<const Box> StorageSystem::GetBox(int i) const {
        if (IsActiveBox(i)) {
            return _activeBox;
        } else {
            Expects(0 <= i && i < _inactiveBoxes.size());
            return _inactiveBoxes[i];
        }
    }

    sol::table StorageSystem::GetPcStorage(sol::this_state lua) const {
        auto count = GetPcBoxCount();
        sol::table result{lua, sol::new_table{count}};
        for (int i = 0; i < count; ++i) {
            // no std::copy because sol doesn't provide good iterators
            result[i + 1] = GetBox(i);
        }
        return std::move(result);
    }

    sol::variadic_results StorageSystem::WithBoxAt(
        sol::object boxObj, sol::variadic_args keys, sol::this_state lua) const {
        if (!boxObj.is<Box>()) {
            return LuaError(lua, "box is not a Monlib::Games::Gen1::Box");
        }
        auto box = boxObj.as<std::shared_ptr<const Box>>();
        auto i = [&]() -> int {
            auto firstType = keys.get_type(0);
            if (firstType == sol::type::number) {
                return keys.get<int>(0) - 1;
            } else if (firstType == sol::type::string) {
                auto key = keys.get<std::string>(0);
                if (key == "team") {
                    return GetSize() - 2;
                } else if (key == "daycare") {
                    return GetSize() - 1;
                } else if (key == "pcStorage"
                    && keys.get_type(1) == sol::type::number) {
                    return keys.get<int>(1);
                }
            }
            return -1;
        }();
        if (i < 0 || GetSize() <= i) {
            return LuaError(lua, "keys do not refer to a box");
        }
        auto newStorage = [&]() -> std::shared_ptr<const StorageSystem> {
            if (IsTeam(i)) {
                if (_team->HasSameStructureAs(*box)) {
                    return std::make_shared<const StorageSystem>(_inactiveBoxes,
                        _activeBox,
                        box,
                        _daycare,
                        _currentBox,
                        _allBoxesChecksums,
                        _mappers);
                } else {
                    return nullptr;
                }
            } else if (IsDaycare(i)) {
                if (_daycare->HasSameStructureAs(*box)) {
                    return std::make_shared<const StorageSystem>(_inactiveBoxes,
                        _activeBox,
                        _team,
                        box,
                        _currentBox,
                        _allBoxesChecksums,
                        _mappers);
                } else {
                    return nullptr;
                }
            } else if (IsActiveBox(i)) {
                if (_activeBox->HasSameStructureAs(*box)) {
                    return std::make_shared<const StorageSystem>(_inactiveBoxes,
                        box,
                        _team,
                        _daycare,
                        _currentBox,
                        _allBoxesChecksums,
                        _mappers);
                } else {
                    return nullptr;
                }
            } else {
                if (_inactiveBoxes[i]->HasSameStructureAs(*box)) {
                    int bank = _inactiveBoxes.size() / 2 <= i;
                    auto checksum =
                        std::to_integer<uint8_t>(_allBoxesChecksums[bank]);
                    auto boxChecksum = _inactiveBoxes[i]->GetChecksum();
                    checksum += std::to_integer<uint8_t>(*boxChecksum);
                    checksum -= std::to_integer<uint8_t>(*box->GetChecksum());
                    auto allBoxesChecksums = _allBoxesChecksums;
                    allBoxesChecksums[bank] = std::byte{checksum};
                    auto inactiveBoxes = copy_shared_dynarray(_inactiveBoxes);
                    inactiveBoxes[i] = box;
                    return std::make_shared<const StorageSystem>(
                        std::move(inactiveBoxes),
                        _activeBox,
                        _team,
                        _daycare,
                        _currentBox,
                        allBoxesChecksums,
                        _mappers);
                } else {
                    return nullptr;
                }
            }
        }();
        return {sol::object{lua, sol::in_place, std::move(newStorage)}};
    }

    std::byte StorageSystem::GetCurrentBoxByte() const { return _currentBox; }

    dynarray<std::byte> StorageSystem::GetDaycareBytes() const {
        return _daycare->GetBytes();
    }
    dynarray<std::byte> StorageSystem::GetTeamBytes() const {
        Expects(_team);
        return _team->GetBytes();
    }
    dynarray<std::byte> StorageSystem::GetActiveBoxBytes() const {
        return _activeBox->GetBytes();
    }
    dynarray<dynarray<std::byte>> StorageSystem::GetBoxBankBytes(int bank) const {
        using std::begin, std::end;
        Expects(0 <= bank && bank < 2);
        auto chunks =
            make_dynarray<dynarray<std::byte>>(_inactiveBoxes.size() / 2 + 1);
        auto checksums = make_dynarray<std::byte>(1 + _inactiveBoxes.size() / 2);
        checksums[0] = _allBoxesChecksums[bank];
        for (int i = 0; i < _inactiveBoxes.size() / 2; ++i) {
            auto box = _inactiveBoxes[i + _inactiveBoxes.size() / 2 * bank];
            Expects(box);
            chunks[i] = box->GetBytes();
            auto checksum = box->GetChecksum();
            Expects(checksum);
            checksums[i + 1] = *checksum;
        }
        chunks[chunks.size() - 1] = std::move(checksums);
        return std::move(chunks);
    }

    sol::usertype<StorageSystem> MakeStorageSystemUsertype() {
        // clang-format off
        return {
            sol::meta_function::index, &StorageSystem::At,
            sol::meta_function::length, &StorageSystem::GetSize,
            "team", sol::property(&StorageSystem::GetTeam),
            "daycare", sol::property(&StorageSystem::GetDaycare),
            "pcStorage", sol::property(&StorageSystem::GetPcStorage),
            "withBoxAt", &StorageSystem::WithBoxAt,
        };
        // clang-format on
    }
}
