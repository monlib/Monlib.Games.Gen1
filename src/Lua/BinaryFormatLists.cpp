#include "Monlib/Games/Gen1/Lua/BinaryFormatLists.h"

#include <memory>

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Lua/ConcatBinaryFormat.h"
#include "Monlib/Games/Gen1/Lua/Pk1BinaryFormat.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {
        const auto UsTeamBinaryFormats = [] {
            BinaryFormatList l{};
            l.Register(UsTeamBinaryFormat,
                std::make_unique<ConcatBinaryFormat>(MonsterStorageType::Team));
            l.Register(UsPk1BinaryFormat, std::make_unique<Pk1BinaryFormat>());
            return l;
        }();
        const auto UsBoxBinaryFormats = [] {
            BinaryFormatList l{};
            l.Register(UsBoxBinaryFormat,
                std::make_unique<ConcatBinaryFormat>(MonsterStorageType::Box));
            return l;
        }();
    }

    const BinaryFormatList& BinaryFormats(MonsterStorageType t) {
        switch (t) {
        case MonsterStorageType::Box: return UsBoxBinaryFormats;
        case MonsterStorageType::Team: return UsTeamBinaryFormats;
        }
        Expects(false);  // Invalid MonsterStorageType
    }
}
