#include "Monlib/Games/Gen1/Lua/Box.h"

#include "Monlib/Games/Gen1/Algorithms.h"
#include "Monlib/Games/Gen1/Interpret/Box.h"
#include "Monlib/Games/Gen1/Interpret/Daycare.h"
#include "Monlib/Games/Gen1/Lua/BinaryFormatLists.h"
#include "Monlib/Games/Gen1/MakeVisitor.h"
#include "Monlib/Games/Gen1/Range.h"

namespace Monlib::Games::Gen1::Lua {
    namespace {}

    Box::Box(Data data, Bytes::Region region, std::shared_ptr<Mappers> mappers) :
        _data{std::move(data)},
        _region{region},
        _mappers{mappers} {
        Expects(mappers != nullptr);
    }

    Box::Box(const Bytes::DaycareBlock& daycare, std::shared_ptr<Mappers> mappers) :
        Box(Destructure(daycare, mappers), daycare.GetRegion(), mappers) {}

    Box::Box(const Bytes::MonsterList& list,
        std::optional<std::byte> checksum,
        std::shared_ptr<Mappers> mappers) :
        Box(Destructure(list, checksum, mappers), list.GetRegion(), mappers) {}

    Box::Data Box::Destructure(
        const Bytes::DaycareBlock& daycare, std::shared_ptr<Mappers> mappers) {
        auto monster = daycare.GetMonster();
        auto nickname = daycare.GetNickname();
        auto otName = daycare.GetOtName();
        return DestructuredDaycare{
            std::make_shared<const Monster>(monster, nickname, otName, mappers),
            daycare.GetOccupied()};
    }

    Box::Data Box::Destructure(const Bytes::MonsterList& list,
        std::optional<std::byte> checksum,
        std::shared_ptr<Mappers> mappers) {
        auto mons =
            make_dynarray<std::shared_ptr<const Monster>>(list.GetCapacity());
        for (int i = 0; i < list.GetCapacity(); ++i) {
            auto monster = list.GetMonster(i);
            auto nickname = list.GetNickname(i);
            auto otName = list.GetOtName(i);
            mons[i] =
                std::make_shared<const Monster>(monster, nickname, otName, mappers);
        }
        auto species = copy_unique_dynarray(list.GetSpeciesList());
        return DestructuredMonsterList{
            std::move(mons),
            std::move(species),
            list.GetUsedEntries(),
            checksum,
        };
    }

    std::shared_ptr<const Monster> Box::At(sol::object o) const {
        if (o.is<int>()) {
            return AtInteger(o.as<int>() - 1);
        } else {
            return nullptr;
        }
    }

    std::shared_ptr<const Monster> Box::AtInteger(int index) const {
        if (0 <= index && index < GetCurrentLength()) {
            overloaded visitor{
                [index](const DestructuredMonsterList& l) { return l.mons[index]; },
                [index](DestructuredDaycare b) { return b.mon; },
            };
            return std::visit(visitor, _data);
        } else {
            return nullptr;
        }
    }

    int Box::GetCurrentLength() const {
        return std::visit(
            overloaded{
                [](const DestructuredMonsterList& list) -> int {
                    return Interpret::BoxEnd(list.species);
                },
                [](const DestructuredDaycare& daycare) -> int {
                    return Interpret::DaycareIsOccupied(daycare.occupiedByte);
                },
            },
            _data);
    }

    int Box::GetCapacity() const {
        overloaded visitor{
            [](const DestructuredMonsterList& list) -> int {
                return list.mons.size();
            },
            [](const DestructuredDaycare&) -> int { return 1; },
        };
        return std::visit(visitor, _data);
    }

    sol::table Box::GetAcceptedBinaryFormats(sol::this_state s) const {
        return BinaryFormats(GetStorageType()).GetTags(s);
    }

    sol::variadic_results Box::WithMonAt(
        sol::object monsterObj, sol::object indexObj, sol::this_state lua) const {
        // co_await with a monadic std::optional would remove a lot of ifs (come back
        // in 2020)
        if (!indexObj.is<int>()) {
            return LuaError(lua, "index is not an integer");
        }
        auto index = indexObj.as<int>() - 1;
        if (monsterObj.is<sol::nil_t>()) {
            return WithIndexCleared(lua, index);
        } else if (monsterObj.is<sol::table>()) {
            return WithMonsterAt(lua, std::move(monsterObj), index);
        } else {
            return LuaError(lua, "monster is not a Monster object");
        }
    }

    sol::variadic_results Box::WithIndexCleared(
        sol::this_state lua, int index) const {
        if (0 <= index && index < GetCurrentLength() - 1) {
            return LuaError(lua, "Result would not be a sequence");
        }
        if (IsTeam() && index == 0) {
            return LuaError(lua, "Result would be an empty team");
        }
        if (index == GetCurrentLength() - 1) {
            overloaded visitor{
                [&](const DestructuredMonsterList& list) -> Data {
                    auto species = copy_unique_dynarray(list.species);
                    auto mons = copy_unique_dynarray(list.mons);
                    auto checksum = list.checksum;
                    species[index] = Bytes::SpeciesListTerminator;
                    auto occupiedByte = std::byte(Interpret::BoxEnd(species));
                    if (checksum) {
                        auto sum = std::to_integer<int>(*checksum);
                        sum += std::to_integer<int>(list.species[index]);
                        sum += std::to_integer<int>(list.usedEntries);
                        sum -= std::to_integer<int>(species[index]);
                        sum -= std::to_integer<int>(occupiedByte);
                        checksum = std::byte(sum);
                    }
                    return DestructuredMonsterList{
                        std::move(mons),
                        std::move(species),
                        occupiedByte,
                        checksum,
                    };
                },
                [&](const DestructuredDaycare&) -> Data {
                    throw std::runtime_error(
                        "daycare modification is not implemented");
                },
            };
            return {sol::object{lua,
                sol::in_place,
                std::make_shared<Box>(
                    std::visit(visitor, _data), _region, _mappers)}};
        } else {
            // Inserting nil where there already is nil is a no-op.
            return {sol::object{lua, sol::in_place, shared_from_this()}};
        }
    }

    sol::variadic_results Box::WithMonsterAt(
        sol::this_state lua, sol::object monsterObj, int index) const {
        auto monster = [&]() {
            if (monsterObj.is<Monster>()) {
                return monsterObj.as<std::shared_ptr<const Monster>>();
            } else {
                sol::table monsterT = monsterObj.as<sol::table>();
                sol::table supportedFormats = monsterT["binaryFormats"];
                sol::function serializeTo = monsterT["serializeTo"];
                for (int i = 0; i < gsl::narrow<int>(supportedFormats.size()); ++i) {
                    LuaBinaryData format = supportedFormats[i];
                    const auto& formats = BinaryFormats(GetStorageType());
                    if (formats.Contains(BinaryFormatTag{format})) {
                        LuaBinaryData serialized = serializeTo(monsterT, format);
                        return formats.Deserialize(
                            BinaryFormatTag{format}, serialized, _mappers);
                    }
                }
                return std::shared_ptr<const Monster>(nullptr);
            }
        }();
        if (!monster) {
            return LuaError(lua, "Could not deserialize monster");
        }
        if (index < 0 || GetCurrentLength() < index || GetCapacity() <= index) {
            return LuaError(
                lua, "Result would not be a sequence with length <= capacity");
        }
        overloaded visitor{
            [&](const DestructuredMonsterList& list) -> Data {
                auto mons = copy_unique_dynarray(list.mons);
                auto species = copy_unique_dynarray(list.species);
                auto checksum = list.checksum;
                mons[index] = monster;
                species[index] = monster->GetMonsterBytes().GetSpecies();
                if (index == GetCurrentLength()) {
                    species[index + 1] = Bytes::SpeciesListTerminator;
                }
                auto occupiedByte = std::byte(Interpret::BoxEnd(species));
                if (checksum) {
                    auto sum = std::to_integer<int>(*checksum);
                    sum += accumulate_bytes(list.species, 0);
                    sum -= accumulate_bytes(species, 0);
                    sum += std::to_integer<int>(list.usedEntries);
                    sum -= std::to_integer<int>(occupiedByte);
                    sum += list.mons[index]->SumOfBytes();
                    sum -= monster->SumOfBytes();
                    checksum = std::byte(sum);
                }
                return DestructuredMonsterList{
                    std::move(mons),
                    std::move(species),
                    occupiedByte,
                    checksum,
                };
            },
            [&](const DestructuredDaycare&) -> Data {
                throw std::runtime_error(
                    "Daycare modification is not yet implemented");
            },
        };
        return {sol::object{lua,
            sol::in_place,
            std::make_shared<Box>(std::visit(visitor, _data), _region, _mappers)}};
    }

    bool Box::IsTeam() const { return GetStorageType() == MonsterStorageType::Team; }

    bool Box::HasSameStructureAs(const Box& box) const {
        return this->IsTeam() == box.IsTeam()
            && this->_data.index() == box._data.index()
            && this->_region == box._region;
    }

    std::optional<std::byte> Box::GetChecksum() const {
        return std::visit(
            overloaded{
                [](const DestructuredMonsterList& l) { return l.checksum; },
                [](const DestructuredDaycare&) {
                    return std::optional<std::byte>{};
                },
            },
            _data);
    }

    MonsterStorageType Box::GetStorageType() const {
        overloaded v{
            [](const DestructuredMonsterList& l) {
                if (l.mons.size() == Bytes::TeamCapacity) {
                    return MonsterStorageType::Team;
                } else {
                    return MonsterStorageType::Box;
                }
            },
            [](const DestructuredDaycare&) { return MonsterStorageType::Box; },
        };
        return std::visit(v, _data);
    }

    dynarray<std::byte> Box::GetBytes() const {
        overloaded v{
            [&](const DestructuredMonsterList& list) {
                using std::begin, std::end;
                auto result = make_dynarray<std::byte>(
                    Bytes::MonsterListSize(GetRegion(), list.mons.size()));
                auto curr = begin(result);
                std::array usedEntries{list.usedEntries};
                curr = std::copy(begin(usedEntries), end(usedEntries), curr);
                curr = std::copy(begin(list.species), end(list.species), curr);
                for (auto& mon : list.mons) {
                    auto bytes = mon->GetMonsterBytes().GetData();
                    curr = std::copy(begin(bytes), end(bytes), curr);
                }
                for (auto& mon : list.mons) {
                    auto bytes = mon->GetOtNameBytes();
                    curr = std::copy(begin(bytes), end(bytes), curr);
                }
                for (auto& mon : list.mons) {
                    auto bytes = mon->GetNicknameBytes();
                    curr = std::copy(begin(bytes), end(bytes), curr);
                }
                Expects(curr == end(result));
                return std::move(result);
            },
            [&](const DestructuredDaycare& daycare) {
                using std::begin, std::end;
                auto result = make_dynarray<std::byte>(Bytes::DaycareBlockSize(
                    GetRegion()));  // Produces 2d too many bytes
                std::array occupied{daycare.occupiedByte};
                auto curr = begin(result);
                auto mon = daycare.mon->GetMonsterBytes().GetData();
                auto ot = daycare.mon->GetOtNameBytes();
                auto nick = daycare.mon->GetNicknameBytes();
                curr = std::copy(begin(occupied), end(occupied), curr);
                curr = std::copy(begin(nick), end(nick), curr);
                curr = std::copy(begin(ot), end(ot), curr);
                curr = std::copy(begin(mon), end(mon), curr);
                Expects(curr == end(result));
                return std::move(result);
            },
        };
        return std::visit(v, _data);
    }

    Bytes::Region Box::GetRegion() const { return _region; }

    sol::usertype<Box> MakeBoxUsertype() {
        // clang-format off
        return {
            sol::meta_function::index, &Box::At,
            "capacity", sol::property(&Box::GetCapacity),
            "acceptedBinaryFormats", sol::property(&Box::GetAcceptedBinaryFormats),
            "withMonAt", &Box::WithMonAt,
        };
        // clang-format on
    }
}
