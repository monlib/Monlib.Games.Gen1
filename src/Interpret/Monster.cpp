#include "Monlib/Games/Gen1/Interpret/Monster.h"

#include "Monlib/Games/Gen1/Interpret/Endian.h"

namespace Monlib::Games::Gen1::Interpret {
    namespace {}

    int GetMonsterLevel(const Bytes::Monster& monster) {
        // Mismatching experience and level are ignored.
        auto levelByte = monster.GetTeamLevel().value_or(monster.GetPcLevel());
        return std::to_integer<int>(levelByte);
    }

    int GetMonsterOtId(const Bytes::Monster& monster) {
        return Interpret::BigEndianToU32(monster.GetOtId());
    }

    int GetMonsterExp(const Bytes::Monster& monster) {
        return Interpret::BigEndianToU32(monster.GetExp());
    }

    void SetMonsterOtId(Bytes::MutableMonster monster, int value) {
        monster.SetOtId(U16ToBigEndian(value));
    }

    bool IsTeamMonster(const Bytes::Monster& m) {
        return m.GetData().size() == Bytes::TeamMonsterSize;
    }

    bool IsBoxMonster(const Bytes::Monster& m) {
        return m.GetData().size() == Bytes::BoxedMonsterSize;
    }
}
