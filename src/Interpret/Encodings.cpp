#include "Monlib/Games/Gen1/Interpret/Encodings.h"

#include <map>

namespace Monlib::Games::Gen1::Interpret {
    namespace {
        const std::map<uint8_t, char> EnToAscii{
            {0x7F, ' '},
            {0x9A, '('},
            {0x9B, ')'},
            {0x9C, ':'},
            {0x9D, ';'},
            {0x9E, '['},
            {0x9F, ']'},
            {0xE0, '\''},
            {0xE3, '-'},
            {0xE6, '?'},
            {0xE7, '!'},
            {0xE8, '.'},
            {0xF2, '.'},
            {0xF3, '/'},
            {0xF4, ','},
        };

        const std::map<uint8_t, const char*> EnToUtf8{
            {0xE1, u8"\U000FA000" /* PK */},
            {0xE2, u8"\U000FA001" /* MN */},
            {0xEC, u8"\u25B7" /* ▷ */},
            {0xED, u8"\u25B6" /* ▶ */},
            {0xEE, u8"\u25BC" /* ▼ */},
            {0xEF, u8"\u2642" /* ♂ */},
            {0xF0, u8"\U000FA002" /* PokéDollar */},
            {0xF1, u8"\u00D7" /* × */},
            {0xF5, u8"\u2640" /* ♀ */},
        };
    }

    std::optional<std::string> Utf8FromEnglishBytes(
        gsl::span<const std::byte> encodedString) {
        std::string decodedString{};
        decodedString.reserve(encodedString.size());
        for (const auto encodedByte : encodedString) {
            auto encodedChar = std::to_integer<uint8_t>(encodedByte);
            if (0x80 <= encodedChar && encodedChar <= 0x99) {
                decodedString.push_back('A' + (encodedChar - 0x80));
            } else if (0xA0 <= encodedChar && encodedChar <= 0xB9) {
                decodedString.push_back('a' + (encodedChar - 0xA0));
            } else if (0xF6 <= encodedChar) {
                decodedString.push_back('0' + (encodedChar - 0xF6));
            } else if (EnToAscii.count(encodedChar) == 1) {
                decodedString.push_back(EnToAscii.at(encodedChar));
            } else if (EnToUtf8.count(encodedChar) == 1) {
                decodedString.append(EnToUtf8.at(encodedChar));
            } else if (encodedChar == 0x50) {
                break;
            } else {
                return std::nullopt;
            }
        }
        return decodedString;
    }

    std::optional<std::vector<std::byte>> EnglishBytesFromUtf8(std::string_view) {
        return std::nullopt;
    }
}
