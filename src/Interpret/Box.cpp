#include "Monlib/Games/Gen1/Interpret/Box.h"

namespace Monlib::Games::Gen1::Interpret {
    int BoxEnd(gsl::span<const std::byte> species) {
        using std::begin, std::end;
        auto endNoTerminator = end(species) - 1;
        auto term =
            std::find(begin(species), endNoTerminator, Bytes::SpeciesListTerminator);
        return gsl::narrow<int>(term - begin(species));
    }
}
