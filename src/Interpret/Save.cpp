#include "Monlib/Games/Gen1/Interpret/Save.h"

#include <numeric>

#include "Monlib/Games/Gen1/Dynarray.h"
#include "Monlib/Games/Gen1/Interpret/Exceptions.h"

namespace Monlib::Games::Gen1::Interpret {
    Bytes::MonsterList GetTeam(Bytes::Save save) {
        return Bytes::MonsterList{
            save.GetTeamBytes(),
            Bytes::TeamCapacity,
            save.GetRegion(),
        };
    }

    Bytes::DaycareBlock GetDaycare(Bytes::Save save) {
        return Bytes::DaycareBlock{
            save.GetDaycareBytes(),
            save.GetRegion(),
        };
    }

    std::optional<Bytes::MonsterList> GetBox(Bytes::Save save, int index) {
        auto region = save.GetRegion();
        auto activeBox = ActiveBoxIndex(save);
        if (0 <= index && index < Bytes::GetBoxCount(region) && activeBox) {
            return Bytes::MonsterList{
                (index == *activeBox) ? save.GetCurrentBoxBytes()
                                      : save.GetUncheckedBoxBytes(index),
                GetBoxCapacity(region),
                region,
            };
        } else {
            return std::nullopt;
        }
    }

    std::optional<int> ActiveBoxIndex(const Bytes::Save& save) {
        auto rawIndex =
            std::to_integer<int>(std::byte{0x7F} & save.GetCurrentBoxIndex());
        if (0 < rawIndex && rawIndex < GetBoxCount(save.GetRegion())) {
            return rawIndex;
        } else {
            return std::nullopt;
        }
    }

    Bytes::MonsterList GetInactiveBox(const Bytes::Save& save, int index) {
        return {
            save.GetUncheckedBoxBytes(index),
            GetBoxCapacity(save.GetRegion()),
            save.GetRegion(),
        };
    }

    Bytes::MonsterList GetActiveBox(const Bytes::Save& save) {
        return {
            save.GetCurrentBoxBytes(),
            GetBoxCapacity(save.GetRegion()),
            save.GetRegion(),
        };
    }
}
