#include "Monlib/Games/Gen1/Interpret/Daycare.h"

namespace Monlib::Games::Gen1::Interpret {
    bool DaycareIsOccupied(std::byte occupiedByte) {
        return occupiedByte == std::byte{0x01};
    }
}
