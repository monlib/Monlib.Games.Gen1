#include "Monlib/Games/Gen1/Bytes/DaycareBlock.h"

namespace Monlib::Games::Gen1::Bytes {
    namespace {
        constexpr auto OccupiedOffset = 0;
        constexpr auto OccupiedSize = sizeof(std::byte);
        constexpr auto NicknameOffset = OccupiedOffset + OccupiedSize;
        constexpr auto NicknameSize(Region region) {
            return GetNicknameLength(region);
        }
        constexpr auto OtNameOffset(Region region) {
            return NicknameOffset + NicknameSize(region);
        }
        constexpr auto OtNameSize = NicknameSize;
        constexpr auto MonsterOffset(Region region) {
            return OtNameOffset(region) + OtNameSize(region);
        }
        constexpr auto DaycareMonsterSize = BoxedMonsterSize;
    }

    DaycareBlock::DaycareBlock(
        shared_dynarray<const std::byte> data, Region region) :
        _data{std::move(data)},
        _region{region} {
        Expects(_data != nullptr && _data.size() == DaycareBlockSize(_region));
    }

    const std::byte& DaycareBlock::GetOccupied() const {
        return _data[OccupiedOffset];
    }
    shared_dynarray<const std::byte> DaycareBlock::GetNickname() const {
        return slice_dynarray(_data, NicknameOffset, NicknameSize(_region));
    }
    shared_dynarray<const std::byte> DaycareBlock::GetOtName() const {
        return slice_dynarray(_data, OtNameOffset(_region), OtNameSize(_region));
    }
    shared_dynarray<const std::byte> DaycareBlock::GetMonster() const {
        return slice_dynarray(_data, MonsterOffset(_region), DaycareMonsterSize);
    }

    Region DaycareBlock::GetRegion() const { return _region; }
    shared_dynarray<const std::byte> DaycareBlock::GetData() const { return _data; }
}
