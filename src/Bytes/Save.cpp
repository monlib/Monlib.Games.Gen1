#include "Monlib/Games/Gen1/Bytes/Save.h"

#include <fstream>

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Bytes/DaycareBlock.h"
#include "Monlib/Games/Gen1/Bytes/Monster.h"
#include "Monlib/Games/Gen1/Bytes/MonsterList.h"

namespace Monlib::Games::Gen1::Bytes {
    namespace {
        struct Offsets {
            int trainerName;
            int mainData;
            int daycare;
            int teamList;
            int currentBoxIndex;
            int currentBoxList;
            int tilesetByte;
            int checksumOffset;
            int firstBox;
            int bank2Checksums;
            int halfBox;
            int bank3Checksums;
        };

        // Not using C++20 yet, but this is better than just a list of random
        // numbers. clang and gcc support it, and that's good enough for now.
        constexpr Offsets EnglishOffsets = {
            .trainerName = 0x2598,
            .mainData = 0x25A3,
            .daycare = 0x2CF4,
            .teamList = 0x2F2C,
            .currentBoxIndex = 0x284C,
            .currentBoxList = 0x30C0,
            .tilesetByte = 0x3522,
            .checksumOffset = 0x3523,
            .firstBox = 0x4000,
            .bank2Checksums = 0x5A4C,
            .halfBox = 0x6000,
            .bank3Checksums = 0x7A4C,
        };

        constexpr Offsets JapaneseOffsets = {
            .trainerName = 0x2598,
            .mainData = 0x259E,  // Guessed
            .daycare = 0x2CA7,
            .teamList = 0x2ED5,
            .currentBoxIndex = 0x2842,
            .currentBoxList = 0x302D,
            .tilesetByte = 0x3593,  // Guessed
            .checksumOffset = 0x3594,
            .firstBox = 0x4000,
            .bank2Checksums = 0x5598,  // Guessed
            .halfBox = 0x6000,
            .bank3Checksums = 0x7598,  // Guessed
        };

        constexpr const Offsets& GetOffsets(Region _region) {
            return _region == Region::Japan ? JapaneseOffsets : EnglishOffsets;
        }

        using shared_bytes = Save::shared_bytes;
    }

    Save::Save(shared_dynarray<const std::byte> data, Region region) :
        _data{std::move(data)},
        _region{region} {
        Expects(_data != nullptr);
    }

    shared_bytes Save::GetTrainerNameBytes() const {
        return slice_dynarray(
            _data, GetOffsets(_region).trainerName, GetOtNameLength(_region));
    }
    shared_bytes Save::GetTeamBytes() const {
        return slice_dynarray(_data,
            GetOffsets(_region).teamList,
            MonsterListSize(_region, TeamCapacity));
    }
    shared_bytes Save::GetDaycareBytes() const {
        return slice_dynarray(
            _data, GetOffsets(_region).daycare, DaycareBlockSize(_region));
    }
    shared_bytes Save::GetCurrentBoxBytes() const {
        return slice_dynarray(_data,
            GetOffsets(_region).currentBoxList,
            MonsterListSize(_region, GetBoxCapacity(_region)));
    }
    std::byte Save::GetCurrentBoxIndex() const {
        return _data[GetOffsets(_region).currentBoxIndex];
    }
    shared_bytes Save::GetUncheckedBoxBytes(int index) const {
        auto boxCount = GetBoxCount(_region);
        auto boxSize = MonsterListSize(_region, GetBoxCapacity(_region));
        Expects(0 <= index && index < boxCount);
        if (index < boxCount / 2) {
            auto firstBox = GetOffsets(_region).firstBox;
            return slice_dynarray(_data, firstBox + index * boxSize, boxSize);
        } else {
            auto halfBox = GetOffsets(_region).halfBox;
            return slice_dynarray(
                _data, halfBox + (index - boxCount / 2) * boxSize, boxSize);
        }
    }
    shared_bytes Save::GetChecksummedRegion() const {
        constexpr int sMainDataStart = 0x2598;
        auto sMainDataCheckSum = GetOffsets(_region).checksumOffset;
        return slice_dynarray(
            _data, sMainDataStart, sMainDataCheckSum - sMainDataStart);
    }
    std::byte Save::GetChecksumByte() const {
        return _data[GetOffsets(_region).checksumOffset];
    }
    std::byte Save::GetBoxChecksumByte(int index) const {
        auto boxCount = GetBoxCount(_region);
        Expects(0 <= index && index < boxCount);
        auto baseOfs = index < (boxCount / 2) ? GetOffsets(_region).bank2Checksums
                                              : GetOffsets(_region).bank3Checksums;
        auto boxInBank = index % (boxCount / 2);
        return _data[baseOfs + 1 + boxInBank];
    }
    std::byte Save::GetBoxBankChecksumByte(int index) const {
        Expects(0 <= index && index < 2);
        return _data[index < 1 ? GetOffsets(_region).bank2Checksums
                               : GetOffsets(_region).bank3Checksums];
    }
    shared_bytes Save::GetBank0() const {
        return slice_dynarray(_data, 0x0000, 0x2000);
    }
    shared_bytes Save::GetBank1PreTrash() const {
        return slice_dynarray(
            _data, 0x2000, GetOffsets(_region).trainerName - 0x2000);
    }
    shared_bytes Save::GetSpriteData() const {
        return slice_dynarray(_data, GetOffsets(_region).teamList - 0x200, 0x200);
    }
    shared_bytes Save::GetMainDataWoDaycare() const {
        return slice_dynarray(_data,
            GetOffsets(_region).mainData,
            GetOffsets(_region).daycare - GetOffsets(_region).mainData);
    }
    shared_bytes Save::GetBank1PostTrash() const {
        return slice_dynarray(_data,
            GetOffsets(_region).checksumOffset + 1,
            0x4000 - (GetOffsets(_region).checksumOffset + 1));
    }
    shared_bytes Save::GetBoxBankTrash(int i) const {
        auto bankEnd = 0x2000 * (i + 3);
        auto trashStart = (i ? GetOffsets(_region).bank3Checksums
                             : GetOffsets(_region).bank2Checksums)
            + 1 + GetBoxCount(_region) / 2;
        return slice_dynarray(_data, trashStart, bankEnd - trashStart);
    }

    std::byte Save::GetTilesetByte() const {
        return _data[GetOffsets(_region).tilesetByte];
    }

    std::optional<Save> LoadSave(std::string filename) {
        if (std::ifstream file{filename, std::ios_base::binary}) {
            std::shared_ptr<std::byte[Save::size]> _data{new std::byte[Save::size]};
            file.seekg(0, std::ios::beg);
            file.read(reinterpret_cast<char*>(_data.get()), Save::size);
            if (file.good()) {
                return Save{{Save::size, std::move(_data)}, Region::Usa};
            } else {
                return std::nullopt;
            }
        } else {
            return std::nullopt;
        }
    }

    int GetCurrentBoxOffset(Region _region) {
        return GetOffsets(_region).currentBoxIndex;
    }
}
