#include "Monlib/Games/Gen1/Bytes/MonsterList.h"

namespace Monlib::Games::Gen1::Bytes {
    namespace {
        using shared_bytes = MonsterList::shared_bytes;
        constexpr bool IsTeamImpl(int _capacity) {
            return _capacity == TeamCapacity;
        }
        constexpr auto UsedEntriesIndex = 0;
        constexpr auto UsedEntriesSize = sizeof(std::byte);
        constexpr auto SpeciesListIndex = UsedEntriesIndex + UsedEntriesSize;
        constexpr int SpeciesListSize(int _capacity) { return _capacity + 1; }
        constexpr int MonsterListIndex(int _capacity) {
            return SpeciesListIndex + SpeciesListSize(_capacity);
        }
        constexpr int MonsterListSize(int _capacity) {
            return MonsterSize(IsTeamImpl(_capacity)) * _capacity;
        }
        constexpr int OtNamesIndex(int _capacity) {
            return MonsterListIndex(_capacity) + MonsterListSize(_capacity);
        }
        constexpr int OtNamesSize(int _capacity, Region _region) {
            return _capacity * GetNicknameLength(_region);
        }
        constexpr int NicknamesIndex(int _capacity, Region _region) {
            return OtNamesIndex(_capacity) + OtNamesSize(_capacity, _region);
        }
        constexpr int NicknamesSize(int _capacity, Region _region) {
            return _capacity * GetNicknameLength(_region);
        }
        static_assert(UsedEntriesIndex == 0);
        static_assert(SpeciesListIndex == 1);
        static_assert(MonsterListIndex(6) == 0x0008);
        static_assert(OtNamesIndex(6) == 0x0110);
        static_assert(OtNamesSize(6, Region::Usa) == 66);
        static_assert(NicknamesIndex(6, Region::Usa) == 0x0152);
        static_assert(
            NicknamesIndex(30, Region::Japan) + NicknamesSize(30, Region::Japan)
            == 0x566);
        static_assert(
            (NicknamesIndex(20, Region::Usa) + NicknamesSize(20, Region::Usa)) * 6
            == 0x1A4C);

        bool IndexIsValid(int _capacity, int i) { return 0 <= i && i < _capacity; }
    }

    int MonsterListSize(Region _region, int _capacity) {
        return NicknamesIndex(_capacity, _region)
            + NicknamesSize(_capacity, _region);
    }

    MonsterList::MonsterList(shared_bytes data, int capacity, Region region) :
        _data{std::move(data)},
        _capacity{capacity},
        _region{region} {
        Expects(_data != nullptr);
    }

    const std::byte& MonsterList::GetUsedEntries() const {
        return _data[UsedEntriesIndex];
    }
    shared_bytes MonsterList::GetSpeciesList() const {
        return slice_dynarray(_data, SpeciesListIndex, SpeciesListSize(_capacity));
    }
    shared_bytes MonsterList::GetMonsters() const {
        return slice_dynarray(
            _data, MonsterListIndex(_capacity), MonsterListSize(_capacity));
    }
    shared_bytes MonsterList::GetOtNames() const {
        return slice_dynarray(
            _data, OtNamesIndex(_capacity), OtNamesSize(_capacity, _region));
    }
    shared_bytes MonsterList::GetNicknames() const {
        return slice_dynarray(_data,
            NicknamesIndex(_capacity, _region),
            NicknamesSize(_capacity, _region));
    }
    shared_bytes MonsterList::GetMonster(int i) const {
        Expects(IndexIsValid(_capacity, i));
        auto monSize = MonsterSize(IsTeamImpl(_capacity));
        return slice_dynarray(GetMonsters(), i * monSize, monSize);
    }
    shared_bytes MonsterList::GetOtName(int i) const {
        Expects(IndexIsValid(_capacity, i));
        auto len = GetNicknameLength(_region);
        return slice_dynarray(GetOtNames(), i * len, len);
    }
    shared_bytes MonsterList::GetNickname(int i) const {
        Expects(IndexIsValid(_capacity, i));
        auto len = GetNicknameLength(_region);
        return slice_dynarray(GetNicknames(), i * len, len);
    }

    bool MonsterList::IsTeam() const { return IsTeamImpl(_capacity); }

    shared_bytes MonsterList::GetData() const { return _data; }
    int MonsterList::GetCapacity() const { return _capacity; }
    Region MonsterList::GetRegion() const { return _region; }
}
