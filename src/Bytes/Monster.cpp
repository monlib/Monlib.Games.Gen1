#include "Monlib/Games/Gen1/Bytes/Monster.h"

#include <algorithm>

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Algorithms.h"

namespace Monlib::Games::Gen1::Bytes {
    namespace {
        using std::array, std::byte, std::optional;
        template <int N, class T>
        array<byte, N> ToArray(T&& data, int start) {
            using std::begin, std::end;
            array<byte, N> out{};
            std::copy(begin(data) + start, begin(data) + start + N, begin(out));
            return out;
        }

        constexpr int StatEvOffset(Stat stat) {
            static_assert(static_cast<int>(Stat::Hp) == 0);
            static_assert(static_cast<int>(Stat::Attack) == 1);
            static_assert(static_cast<int>(Stat::Defense) == 2);
            static_assert(static_cast<int>(Stat::Speed) == 3);
            static_assert(static_cast<int>(Stat::Special) == 4);
            return 2 * static_cast<int>(stat);
        }

        constexpr int TeamStatOffset(Stat stat) { return StatEvOffset(stat); }
    }

    Monster::Monster(shared_dynarray<const byte> data) : _data{data} {
        Expects(data != nullptr
            && (_data.size() == BoxedMonsterSize
                   || _data.size() == TeamMonsterSize));
    }

    MutableMonster::MutableMonster(shared_dynarray<byte> data) :
        Monster{data},
        _mutableData{data} {}
    byte Monster::GetSpecies() const { return _data[0x00]; }
    array<byte, 2> Monster::GetCurrentHP() const { return ToArray<2>(_data, 0x01); }
    byte Monster::GetPcLevel() const { return _data[0x03]; }
    byte Monster::GetStatus() const { return _data[0x04]; }
    array<byte, 2> Monster::GetTypes() const { return ToArray<2>(_data, 0x05); }
    byte Monster::GetCatchRate() const { return _data[0x07]; }
    array<byte, 4> Monster::GetMoves() const { return ToArray<4>(_data, 0x08); }
    array<byte, 2> Monster::GetOtId() const { return ToArray<2>(_data, 0x0C); }
    array<byte, 3> Monster::GetExp() const { return ToArray<3>(_data, 0x0E); }
    array<byte, 2> Monster::GetStatEv(Stat stat) const {
        return ToArray<2>(_data, 0x11 + StatEvOffset(stat));
    }
    array<byte, 2> Monster::GetIvData() const { return ToArray<2>(_data, 0x1B); }
    array<byte, 4> Monster::GetPpValues() const { return ToArray<4>(_data, 0x1D); }
    optional<byte> Monster::GetTeamLevel() const {
        // clang-format off
        return _data.size() == TeamMonsterSize
            ? optional{_data[0x21]}
            : std::nullopt;
        // clang-format on
    }
    optional<array<byte, 2>> Monster::GetMaxHp() const {
        return GetTeamStat(Stat::Hp);
    }
    optional<array<byte, 2>> Monster::GetTeamStat(Stat stat) const {
        return _data.size() == TeamMonsterSize
            ? optional{ToArray<2>(_data, 0x22 + TeamStatOffset(stat))}
            : std::nullopt;
    }
    void MutableMonster::OverwriteWith(const Monster& other) const {
        if (this->_mutableData.size() != other.GetData().size()) {
            throw std::runtime_error("Cannot overwrite with mismatching sizes.");
        }
        CopyIfDifferentRanges(
            begin(other.GetData()), end(other.GetData()), begin(this->_mutableData));
    }
    void MutableMonster::SetOtId(array<byte, 2> otId) const {
        using std::begin, std::end;
        std::copy(begin(otId), end(otId), begin(_mutableData) + 0x0C);
    }
    MutableMonster Monster::Copy() const { return {copy_shared_dynarray(_data)}; }

    shared_dynarray<const byte> Monster::GetData() const { return _data; }
    shared_dynarray<byte> MutableMonster::GetMutableData() const {
        return _mutableData;
    }
}
