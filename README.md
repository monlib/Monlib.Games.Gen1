# Monlib.Games.Gen1
[![pipeline status](https://gitlab.com/monlib/Monlib.Games.Gen1/badges/master/pipeline.svg)](https://gitlab.com/monlib/Monlib.Games.Gen1/commits/master)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/Monlib/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

> Edit First Generation Save Files through Lua

This is a Lua package for editing the save files of first generation Pokemon
games, using the [Monlib.Core API][monlib-core-api]. Using
this package, you can easily write your own save viewers/editors without
reimplementing everything and benefit from community improvements.

This package works with 32/64 bit Lua 5.3. Lua 5.2 probably works, but isn't
checked. This package runs on all systems with a C++17 compatible compiler.

## Disclaimer

Never use cheated save files to ruin other people's fun, especially online.

This project is not in any way affiliated with Nintendo or The Pokémon Company.

## Example

```lua
local gen1 = require('monlib.games.gen1')

local save = gen1.Save.new('mysave.sav', speciesMapper) -- See note
print(save.monsterStorage.pcStorage[2][5].level) --> 23
local mon = save.monsterStorage.team[1]
local newMon = mon.modifiedWith({nickname = "notMEW"})
print(mon.nickname) --> "MEW"
print(newMon.nickname) --> "notMEW"
```

Consult the [documentation][doc-url] for details.

## Getting Started

To use this package, you'll need a build that is compatible with your lua
interpreter, please see the `Compiling` section for details. Once you have
the module `.dll` (or `.so`) save it in a subfolder as `monlib/games/gen1.dll`,
where your interpreter expects it. Then simply run your interpreter and require
the module into a local variable:

```lua
local gen1 = require('monlib.games.gen1')
```

You probably want to open a save file now:

```lua
local save = gen1.Save.new('mySavefile.sav', speciesMapper)
```

`speciesMapper` is a table of gen1 species index numbers to your desired
indexing scheme (Probably national IDs). This function is *very* likely to
change while in development.

## Supported Features

This package supports Red, Green, Blue and Yellow save files from all regions.

- Reading Detailed Stats
- Editing Monsters
- Exporting / Importing Monsters
- Moving / Copying / Deleting Monsters from Storage
- Glitch Monsters
- Editing Save Files in Memory

See the [documentation][doc-url] for details.

## Compiling

These instructions are for Linux. If you are using Windows, look into WSL.
First of all, clone this repo into the current directory (or extract a
source tarball):

```sh
git clone https://gitlab.com/monlib/Monlib.Games.Gen1.git
cd Monlib.Games.Gen1
```

### Setup Using Docker

You can use the CI Docker images for testing purposes, though you might
encounter issues with using the binaries outside of the container. Look into
`.gitlab-ci.yml` for the currently used docker images and run that. Example:

```sh
docker run --rm -itv "$PWD:/work" registry.gitlab.com/monlib/build-image/gcc73:fdb8191
# You should now be in the container
cd /work
```
### Local Setup

You'll
need to install [Conan][conan-installation]. It is recommended to install it using
python3 inside a virtual environment (see [virtualenvwrapper][virtualenvwrapper]).
If you work on other projects that use conan, consider setting `CONAN_USER_HOME`
to some Monlib specific folder.

Now set up the default profile and remotes:

```sh
conan profile new --detect default # Only needed for fresh installs
conan remote add monlib https://api.bintray.com/conan/monlib/Monlib
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

After that, copy [all Monlib profiles][monlib-profiles] to your profiles folder
(either `$CONAN_USER_HOME/.conan/profiles` or `~/.conan/profiles`).

You can now run the compilation commands!

### Compiling for the Binary

Look here, if you simply want to build a `.dll` or `.so` for your Lua version.

```sh
# Linebreaks for readability.
conan install --install-folder build/ --build missing --profile monlib-base \
    -o lua:forModule=True \
    -o Monlib.Games.Gen1:fPIC=True \
    -o Monlib.Games.Gen1:shared=True \
    .
conan build --build-folder build/ .
```

You may need to adjust some options to match your lua version. Use
`-o lua:OPTIONNAME=VALUE` to do so. If you need to change the version,
edit the `Lua` entry in the requires section of `conanfile.py`. Please
look at [lua-conan][lua-conan] for a list of options and versions.

### Compiling to Test

To compile and run the tests for the debug version, using a statically linked
custom Lua version, simply run `./tests.sh`.

## Contributing

Thanks for thinking about contributing! You'll find details in [this file](./CONTRIBUTING.md).

## Thanks

For the knowledge and research:

- All those who made these wonderful games possible
- [SciresM][sciresm] for writing [Rhydon][rhydon], which was a valuable reference
- [Kurt (kwsch)][kwsch] and [contributors][pkhex-contrib] for writing [PKHeX][pkhex], which integrated Rhydon
and also served as a reference
- All [contributors][pokered-contrib] to [pokered][pokered], which provided provided insight into the inner workings of these games
- [Bulbapedia][bulbapedia] for their [technical documentation][bulbapedia-tech-docs] which is very readable, even for beginners
- [Project Pokemon][proj-pkm] for all their research and save editors

Tools:

- [Sol2][sol2], for making the Lua easily usable from C++
- [Conan][conan] for saving me from submodule hell
- [LuaUnit][luaunit], for the Lua tests.
- [Catch][catch], for the C++ tests.

## Versioning

This project uses semantic versioning. See the [documentation][doc-url] for the API.
This package is still in development, as is the [Monlib.Core API][monlib-core-api],
so be ready for breakage until 1.0.0.

## License

This project is licensed under the GPLv3 or higher, unless otherwise noted. A
copy is included in the [LICENSE file](./LICENSE). By contributing to this
project you agree that your work is licensed under the terms of the GPLv3 or
any later version.

The source for [LuaUnit][luaunit] is included. It is licensed under the BSD
License, a copy is available [on github][luaunit-license]. Philippe Fremy holds
the copyright to it.

This project uses [Sol2][sol2] which is licensed under the MIT License. Rapptz, ThePhD,
and contributors hold the copyright to it.

This project uses [Microsoft's GSL][m$-gsl] which is licensed under the MIT License.
Microsoft holds the copyright.

[monlib-core-api]: https://monlibcore.readthedocs.io
[conan-installation]: https://docs.conan.io/en/latest/installation.html
[virtualenvwrapper]: http://virtualenvwrapper.readthedocs.io/en/latest/index.html
[monlib-profiles]: https://gitlab.com/monlib/conan-config/tree/master/profiles
[lua-conan]: https://gitlab.com/monlib/lua-conan
[sciresm]: https://github.com/SciresM
[rhydon]: https://github.com/SciresM/Rhydon
[kwsch]: https://github.com/kwsch
[pkhex-contrib]: https://github.com/kwsch/PKHeX/graphs/contributors
[pkhex]: https://github.com/kwsch/PKHeX
[pokered-contrib]: https://github.com/pret/pokered/graphs/contributors
[pokered]: https://github.com/pret/pokered
[bulbapedia]: https://bulbapedia.bulbagarden.net
[bulbapedia-tech-docs]: https://bulbapedia.bulbagarden.net/wiki/Save_data_structure_in_Generation_I
[proj-pkm]: https://projectpokemon.org
[luaunit-license]: https://github.com/bluebird75/luaunit/blob/master/LICENSE.txt
[sol2]: https://github.com/ThePhD/sol2
[conan]: https://conan.io
[luaunit]: https://github.com/bluebird75/luaunit
[catch]: https://github.com/catchorg/Catch2
[m$-gsl]: https://github.com/Microsoft/GSL
[doc-url]: https://monlibgamesgen1.readthedocs.io
