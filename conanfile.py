from conans import ConanFile, CMake

class MonlibGamesGen1(ConanFile):
    name = 'Monlib.Games.Gen1'
    license = 'GPLv3+'
    url = 'https://gitlab.com/monlib/Monlib.Games.Gen1.git'
    description = 'A Lua module to edit gen1 save data'
    settings = 'os', 'arch', 'build_type', 'compiler'
    exports_sources = 'src/*', 'include/*', 'CMakeLists.txt'
    requires = (
        'lua/5.3.4@monlib/stable',
        'sol2/2.20.4@monlib/stable',
        'gsl_microsoft/20180102@bincrafters/stable',
        'Monlib.Util/v4@monlib/stable',
    )
    build_requires = 'cmake_installer/[~3.10]@conan/stable'
    generators = 'cmake'
    options = {
        'shared': [True, False],
        'fPIC': [True, False],
    }
    default_options = (
        'shared=False',
        'fPIC=False',
    )

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy('*.h', dst='include', src='include')
        self.copy('*mon-games-gen1.lib', dst='lib', keep_path=False)
        self.copy('*.dll', dst='lib', keep_path=False)
        self.copy('*.so', dst='lib', keep_path=False)
        self.copy('*.dylib', dst='lib', keep_path=False)
        self.copy('*.a', dst='lib', keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ['mon-games-gen1']
        self.cpp_info.defines = ['GSL_THROW_ON_CONTRACT_VIOLATION']
