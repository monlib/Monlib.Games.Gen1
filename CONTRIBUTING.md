# Contributing

Thanks for taking the time to contribute! Issues and Merge Requests are always
appreciated.

## Issues

Use issues for suggesting changes, new features, or pointing out bugs. If you
need support, please head to
[the Gitter chat](https://gitter.im/Monlib/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
first. Please search for similar issues (including closed ones) before opening a new one,
to avoid duplicates.

When filing bug reports, please provide as much relevant information as possible. This includes
commit, operating system, build type, statically or dynamically linked Lua, etc. Please also
include detailed steps on how to reproduce the bug, as well as an example save file or crash dumps
if you have them.

## Merge Requests

First of all, thanks for considering to contribute code! There are a few
guidelines you should follow to get your code merged:

- Keep trademarks out of the codebase.
- Don't hardcode data. Some users might use this project for hacked (or heavily
glitched) games, so the "sane default" should always be configurable (e.g. using NationalIDs for `Monster.species`).
- Write readable and maintainable code. If the maintainers don't know what
this code does now, how is anybody supposed to improve it in 6 months?
- Format your C++ code using `clang-format-6.0` and the provided .clang-format.
- Add tests (Lua or C++) for new features or bug fixes.
- Try to keep your commits atomic and write useful commit messages. See [this
great article]((https://chris.beams.io/posts/git-commit/)) for recommendations.
- Don't modify data unless you have to. Changing data is error prone and the
original is often important to keep around.
