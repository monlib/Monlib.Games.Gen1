#ifndef MONLIB_GAMES_GEN1_ARRAYSPECIESMAPPER_H
#define MONLIB_GAMES_GEN1_ARRAYSPECIESMAPPER_H

#include <array>

#include "Monlib/Games/Gen1/SpeciesMapper.h"

namespace Monlib::Games::Gen1 {
    struct ArraySpeciesMapper {
    public:
        ArraySpeciesMapper(SpeciesId fallback) : _ids{} {
            using std::begin, std::end;
            std::fill(begin(_ids), end(_ids), fallback);
        }

        void RegisterSpecies(SpeciesByte b, SpeciesId i) {
            _ids[std::to_integer<unsigned>(b)] = i;
        }

        SpeciesId operator()(SpeciesByte byte) const {
            return _ids.at(std::to_integer<unsigned>(byte));
        }

    private:
        // Stores species ids, indexed by their species byte's numerical value.
        std::array<SpeciesId, 256> _ids;
    };
}

#endif
