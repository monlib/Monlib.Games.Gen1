#ifndef MONLIB_GAMES_GEN1_ALGORITHMS_H
#define MONLIB_GAMES_GEN1_ALGORITHMS_H

#include <algorithm>

namespace Monlib::Games::Gen1 {
    template <class InputIt, class OutputIt>
    constexpr OutputIt CopyIfDifferentRanges(
        InputIt first, InputIt last, OutputIt d_first) {
        if (first != d_first) {
            return std::copy(first, last, d_first);
        } else {
            return d_first;
        }
    }
}

#endif
