#ifndef MONLIB_GAMES_GEN1_RANGES_H
#define MONLIB_GAMES_GEN1_RANGES_H

#include <cstddef>
#include <type_traits>
#include <utility>

#include "Monlib/Util/EasyRange.h"

namespace Monlib::Games::Gen1 {
    using Util::EasyRange;

    template <class R, class T>
    T accumulate_bytes(R&& byteRange, T&& init) {
        for (std::byte b : byteRange) {
            init = std::move(init) + std::to_integer<T>(b);
        }
        return init;
    }
}

#endif
