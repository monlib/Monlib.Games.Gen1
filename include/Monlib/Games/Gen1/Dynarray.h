#ifndef MONMAN_NDS_UTILITY_DYNARRAY_H
#define MONMAN_NDS_UTILITY_DYNARRAY_H

#include "Monlib/Util/Dynarray.h"

namespace Monlib::Games::Gen1 {
    using Util::copy_shared_dynarray;
    using Util::copy_unique_dynarray;
    using Util::dynarray;
    using Util::make_dynarray;
    using Util::make_shared_dynarray;
    using Util::shared_dynarray;
    using Util::slice_dynarray;
}

#endif
