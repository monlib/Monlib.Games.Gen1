#ifndef MONLIB_GAMES_GEN1_INTERPRET_ENDIAN_H
#define MONLIB_GAMES_GEN1_INTERPRET_ENDIAN_H

#include <array>
#include <cstddef>
#include <cstdint>
#include <type_traits>

#include <gsl/gsl>

namespace Monlib::Games::Gen1::Interpret {

    template <std::size_t N>
    constexpr std::enable_if_t<0 < N && N <= 4, uint_least32_t> BigEndianToU32(
        std::array<std::byte, N> bytes) {
        int result = 0;
        auto initialShift = (N - 1) * 8;
        for (std::size_t i = 0; i < bytes.size(); ++i) {
            result |= std::to_integer<uint_least32_t>(bytes[i])
                << (initialShift - i * 8);
        }
        return result;
    }

    constexpr std::array<std::byte, 2> U16ToBigEndian(int in) {
        Expects(0 <= in && in <= 0xFFFF);
        return {std::byte(in / 0x100), std::byte(in % 0x100)};
    }
}

#endif
