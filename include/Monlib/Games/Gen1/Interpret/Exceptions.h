#ifndef MONLIB_GAMES_GEN1_INTERPRET_EXCEPTIONS_H
#define MONLIB_GAMES_GEN1_INTERPRET_EXCEPTIONS_H

#include <stdexcept>

namespace Monlib::Games::Gen1::Interpret {
    struct bad_save_file : public std::runtime_error {
        explicit bad_save_file(const char* m) : std::runtime_error(m) {}
        explicit bad_save_file(const std::string& m) : std::runtime_error(m) {}
    };
}

#endif
