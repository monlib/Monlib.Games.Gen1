#ifndef MONLIB_GAMES_GEN1_INTERPRET_BOX_H
#define MONLIB_GAMES_GEN1_INTERPRET_BOX_H

#include <algorithm>
#include <cstddef>
#include <memory>
#include <optional>

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Algorithms.h"
#include "Monlib/Games/Gen1/Bytes/MonsterList.h"

namespace Monlib::Games::Gen1::Interpret {
    int BoxEnd(gsl::span<const std::byte> species);
}

#endif
