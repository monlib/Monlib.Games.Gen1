#ifndef MONLIB_GAMES_GEN1_INTERPRET_DAYCARE_H
#define MONLIB_GAMES_GEN1_INTERPRET_DAYCARE_H

#include <cstddef>

#include "Monlib/Games/Gen1/Bytes/DaycareBlock.h"
namespace Monlib::Games::Gen1::Interpret {
    bool DaycareIsOccupied(std::byte occupiedByte);
}

#endif
