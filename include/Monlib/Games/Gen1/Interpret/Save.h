#ifndef MONLIB_GAMES_GEN1_INTERPRET_SAVE_H
#define MONLIB_GAMES_GEN1_INTERPRET_SAVE_H

#include "Monlib/Games/Gen1/Bytes/DaycareBlock.h"
#include "Monlib/Games/Gen1/Bytes/MonsterList.h"
#include "Monlib/Games/Gen1/Bytes/Save.h"

namespace Monlib::Games::Gen1::Interpret {
    Bytes::MonsterList GetTeam(Bytes::Save save);
    Bytes::DaycareBlock GetDaycare(Bytes::Save save);
    std::optional<Bytes::MonsterList> GetBox(Bytes::Save save, int index);
    std::optional<int> ActiveBoxIndex(const Bytes::Save&);
    Bytes::MonsterList GetInactiveBox(const Bytes::Save&, int);
    Bytes::MonsterList GetActiveBox(const Bytes::Save&);
}

#endif
