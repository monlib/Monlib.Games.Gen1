#ifndef MONLIB_GAMES_GEN1_INTERPRET_MONSTER_H
#define MONLIB_GAMES_GEN1_INTERPRET_MONSTER_H

#include <tuple>

#include "Monlib/Games/Gen1/Bytes/Monster.h"

namespace Monlib::Games::Gen1::Interpret {
    int GetMonsterLevel(const Bytes::Monster&);
    int GetMonsterOtId(const Bytes::Monster&);
    int GetMonsterExp(const Bytes::Monster&);
    void SetMonsterOtId(Bytes::MutableMonster, int);
    bool IsTeamMonster(const Bytes::Monster&);
    bool IsBoxMonster(const Bytes::Monster&);
}

#endif
