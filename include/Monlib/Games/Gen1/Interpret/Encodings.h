#ifndef MONLIB_GAMES_GEN1_ENCODINGS_H
#define MONLIB_GAMES_GEN1_ENCODINGS_H

#include <cstdint>
#include <optional>
#include <string_view>
#include <vector>

#include <gsl/gsl>

namespace Monlib::Games::Gen1::Interpret {
    std::optional<std::string> Utf8FromEnglishBytes(gsl::span<const std::byte>);
    std::optional<std::vector<std::byte>> EnglishBytesFromUtf8(std::string_view);
}

#endif
