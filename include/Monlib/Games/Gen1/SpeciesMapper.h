#ifndef MONLIB_GAMES_GEN1_SPECIESMAPPER_H
#define MONLIB_GAMES_GEN1_SPECIESMAPPER_H

#include <cstddef>
#include <functional>

namespace Monlib::Games::Gen1 {
    using SpeciesId = int;
    using SpeciesByte = std::byte;
    using SpeciesMapper = std::function<SpeciesId(SpeciesByte)>;
}

#endif
