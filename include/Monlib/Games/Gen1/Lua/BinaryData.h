#ifndef MONLIB_GAMES_GEN1_LUA_BINARYDATA_H
#define MONLIB_GAMES_GEN1_LUA_BINARYDATA_H

#include <string>

namespace Monlib::Games::Gen1::Lua {
    // Type capable of holding arbitrary bytes for passing into lua.
    using LuaBinaryData = std::string;
}

#endif
