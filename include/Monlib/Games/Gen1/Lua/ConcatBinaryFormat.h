#ifndef MONLIB_GAMES_GEN1_LUA_CONCATBINARYFORMAT_H
#define MONLIB_GAMES_GEN1_LUA_CONCATBINARYFORMAT_H

#include "Monlib/Games/Gen1/Lua/BinaryFormats.h"
#include "Monlib/Games/Gen1/Lua/Mappers.h"
#include "Monlib/Games/Gen1/Lua/MonsterStorageType.h"

namespace Monlib::Games::Gen1::Lua {
    constexpr BinaryFormatTag UsTeamBinaryFormat =
        "0c4662ce-6df3-4a83-8c6d-f33fad06ecfa";
    constexpr BinaryFormatTag UsBoxBinaryFormat =
        "cd74317b-895b-4cf6-b07b-5ad93c8505e0";
    constexpr BinaryFormatTag UsDaycareBinaryFormat =
        "4fa61f0a-ecb7-4596-8c27-4108028c3848";

    // Simply concatenates species + monster + otName + nickname
    class ConcatBinaryFormat : public BinaryFormat {
    public:
        ConcatBinaryFormat(MonsterStorageType);
        virtual LuaBinaryData Serialize(const Monster&) const override;
        virtual std::shared_ptr<const Monster> Deserialize(
            const LuaBinaryData&, std::shared_ptr<Mappers>) const override;
        virtual ~ConcatBinaryFormat() = default;

    private:
        MonsterStorageType _storageType;

        int OutputSize() const;
    };
}

#endif
