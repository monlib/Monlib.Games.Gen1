#ifndef MONLIB_GAMES_GEN1_LUA_SAVE_H
#define MONLIB_GAMES_GEN1_LUA_SAVE_H

#include "Monlib/Games/Gen1/Bytes/Save.h"
#include "Monlib/Games/Gen1/Lua/BinaryData.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"
#include "Monlib/Games/Gen1/Lua/StorageSystem.h"

namespace Monlib::Games::Gen1::Lua {
    class Save {
    public:
        Save(const Bytes::Save&, std::shared_ptr<Mappers>);
        std::shared_ptr<const StorageSystem> GetStorageSystem() const;
        std::string SerializeToString() const;
        void SerializeToFile(sol::table) const;
        sol::variadic_results WithMonsterStorage(sol::object, sol::this_state) const;

    private:
        std::shared_ptr<const StorageSystem> _storageSystem;
        // Save must store all data that isn't delegated to other components. Since
        // that data is never changed, the overhead for shared_ptr makes sense.
        shared_dynarray<const std::byte> _bank0;
        shared_dynarray<const std::byte> _bank1PreTrash;
        shared_dynarray<const std::byte> _trainerName;
        shared_dynarray<const std::byte> _spriteData;
        // mainData duplicates daycare and current box
        shared_dynarray<const std::byte> _mainDataWoDaycare;
        std::byte _tilesetType;
        std::byte _mainDataChecksum;
        shared_dynarray<const std::byte> _bank1PostTrash;
        std::array<shared_dynarray<const std::byte>, 2> _boxBanksTrash;
        Bytes::Region _region;
        std::shared_ptr<Mappers> _mappers;

        int GetCurrentBoxOffset() const;
        std::byte CalculateMainDataChecksum() const;
        dynarray<const std::byte> ChecksummedRegion() const;

    public:
        // public because make_shared
        Save(decltype(_storageSystem),
            decltype(_bank0),
            decltype(_bank1PreTrash),
            decltype(_trainerName),
            decltype(_spriteData),
            decltype(_mainDataWoDaycare),
            decltype(_tilesetType),
            std::optional<decltype(_mainDataChecksum)>,  // nullopt = calculate
            decltype(_bank1PostTrash),
            decltype(_boxBanksTrash),
            decltype(_region),
            decltype(_mappers));
    };

    sol::object MakeSave(std::string, sol::table, sol::this_state);

    sol::usertype<Save> MakeSaveUsertype();
}

#endif
