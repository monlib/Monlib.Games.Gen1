#ifndef MONLIB_GAMES_GEN1_LUA_BINARYFORMATS_H
#define MONLIB_GAMES_GEN1_LUA_BINARYFORMATS_H

#include <array>
#include <cstddef>
#include <map>
#include <memory>

#include "Monlib/Games/Gen1/Lua/BinaryData.h"
#include "Monlib/Games/Gen1/Lua/Mappers.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"

namespace Monlib::Games::Gen1::Lua {
    namespace BinaryFormatsHeaderImpl {
        constexpr int HexCharToNibble(char c) {
            if ('0' <= c && c <= '9') {
                return c - '0';
            } else if ('A' <= c && c <= 'F') {
                return c - 'A' + 10;
            } else if ('a' <= c && c <= 'f') {
                return c - 'a' + 10;
            } else {
                throw std::runtime_error("not a niblble!");
            }
        }

        constexpr std::byte CharsToByte(char c0, char c1) {
            return std::byte(HexCharToNibble(c0) << 4 | HexCharToNibble(c1));
        }

        constexpr int IndexToFirstInStr(int i) {
            if (i < 4) {
                return 2 * i;
            } else if (i < 6) {
                return 2 * i + 1;
            } else if (i < 8) {
                return 2 * i + 2;
            } else if (i < 10) {
                return 2 * i + 3;
            } else {
                return 2 * i + 4;
            }
        }
    }

    constexpr std::array<std::byte, 16> UuidStringToBytes(const char* str) {
        namespace details = BinaryFormatsHeaderImpl;
        using details::CharsToByte, details::IndexToFirstInStr;
        std::array<std::byte, 16> bytes{};
        for (int i = 0; i < 16; ++i) {
            auto j = IndexToFirstInStr(i);
            bytes[i] = CharsToByte(str[j], str[j + 1]);
        }
        return bytes;
    }

    class BinaryFormatTag {
    public:
        constexpr BinaryFormatTag(const char* c) : _bytes{UuidStringToBytes(c)} {}
        explicit BinaryFormatTag(LuaBinaryData);
        explicit operator LuaBinaryData() const;

    private:
        std::array<std::byte, 16> _bytes;
        friend bool operator<(BinaryFormatTag, BinaryFormatTag);
    };

    constexpr BinaryFormatTag UsPk1BinaryFormat =
        "74dd0ed6-4da5-44a3-a199-702e6f3d09fe";

    class Monster;

    class BinaryFormat {
    public:
        virtual LuaBinaryData Serialize(const Monster&) const = 0;
        virtual std::shared_ptr<const Monster> Deserialize(
            const LuaBinaryData&, std::shared_ptr<Mappers>) const = 0;
        virtual ~BinaryFormat() = default;
    };

    class BinaryFormatList {
    public:
        LuaBinaryData Serialize(const BinaryFormatTag&, const Monster&) const;
        std::shared_ptr<const Monster> Deserialize(const BinaryFormatTag&,
            const LuaBinaryData&,
            std::shared_ptr<Mappers>) const;
        void Register(BinaryFormatTag, std::unique_ptr<BinaryFormat>);
        sol::table GetTags(sol::state_view) const;
        bool Contains(const BinaryFormatTag&) const;

    private:
        std::map<BinaryFormatTag, std::unique_ptr<BinaryFormat>> _formats;

        const BinaryFormat& GetFormat(const BinaryFormatTag&) const;
    };
}

#endif
