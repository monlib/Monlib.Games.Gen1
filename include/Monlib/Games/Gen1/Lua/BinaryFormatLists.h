#ifndef MONLIB_GAMES_GEN1_LUA_BINARYFORMATLISTS_H
#define MONLIB_GAMES_GEN1_LUA_BINARYFORMATLISTS_H

#include "Monlib/Games/Gen1/Lua/BinaryFormats.h"
#include "Monlib/Games/Gen1/Lua/MonsterStorageType.h"

namespace Monlib::Games::Gen1::Lua {
    const BinaryFormatList& BinaryFormats(MonsterStorageType);
}

#endif
