#ifndef MONLIB_GAMES_GEN1_LUA_BOX_H
#define MONLIB_GAMES_GEN1_LUA_BOX_H

#include <memory>
#include <string>

#include "Monlib/Games/Gen1/Bytes/DaycareBlock.h"
#include "Monlib/Games/Gen1/Bytes/MonsterList.h"
#include "Monlib/Games/Gen1/Lua/Mappers.h"
#include "Monlib/Games/Gen1/Lua/Monster.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"

namespace Monlib::Games::Gen1::Lua {
    // shared_from_this for cheaply handling cases where withMonAt is identity
    class Box : public std::enable_shared_from_this<Box> {
    public:
        Box(const Bytes::DaycareBlock&, std::shared_ptr<Mappers>);
        Box(const Bytes::MonsterList&,
            std::optional<std::byte>,
            std::shared_ptr<Mappers>);
        std::shared_ptr<const Monster> At(sol::object) const;
        std::shared_ptr<const Monster> AtInteger(int) const;
        sol::variadic_results WithMonAt(
            sol::object, sol::object, sol::this_state) const;
        int GetCurrentLength() const;
        int GetCapacity() const;
        sol::table GetAcceptedBinaryFormats(sol::this_state) const;
        std::optional<std::byte> GetChecksum() const;
        bool HasSameStructureAs(const Box&) const;

        dynarray<std::byte> GetBytes() const;

    private:
        struct DestructuredMonsterList {
            // mons aren't shared because it would only improve clear performance, at
            // an additional time and space cost for regular modifications
            dynarray<const std::shared_ptr<const Monster>> mons;
            dynarray<const std::byte> species;
            std::byte usedEntries;
            std::optional<std::byte> checksum;
        };

        struct DestructuredDaycare {
            std::shared_ptr<const Monster> mon;
            std::byte occupiedByte;
        };
        using Data = std::variant<DestructuredMonsterList, DestructuredDaycare>;

        Data _data;
        Bytes::Region _region;
        std::shared_ptr<Mappers> _mappers;

        MonsterStorageType GetStorageType() const;
        bool IsTeam() const;
        sol::variadic_results WithIndexCleared(sol::this_state, int) const;
        sol::variadic_results WithMonsterAt(sol::this_state, sol::object, int) const;
        Bytes::Region GetRegion() const;

        static Data Destructure(
            const Bytes::DaycareBlock&, std::shared_ptr<Mappers>);
        static Data Destructure(const Bytes::MonsterList&,
            std::optional<std::byte>,
            std::shared_ptr<Mappers>);

    public:
        // Should be private, but std::make_shared
        Box(Data, Bytes::Region, std::shared_ptr<Mappers>);
    };

    sol::usertype<Box> MakeBoxUsertype();
}

#endif
