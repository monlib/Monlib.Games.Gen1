#ifndef MONLIB_GAMES_GEN1_LUA_STORAGESYSTEM_H
#define MONLIB_GAMES_GEN1_LUA_STORAGESYSTEM_H

#include <memory>

#include "Monlib/Games/Gen1/Bytes/Save.h"
#include "Monlib/Games/Gen1/Lua/Box.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"

namespace Monlib::Games::Gen1::Lua {
    class StorageSystem {
    public:
        StorageSystem(const Bytes::Save&, std::shared_ptr<Mappers>);
        int GetSize() const;
        std::shared_ptr<const Box> At(sol::object) const;
        std::shared_ptr<const Box> GetTeam() const;
        std::shared_ptr<const Box> GetDaycare() const;
        sol::table GetPcStorage(sol::this_state) const;
        sol::variadic_results WithBoxAt(
            sol::object, sol::variadic_args, sol::this_state) const;

        std::byte GetCurrentBoxByte() const;
        dynarray<std::byte> GetDaycareBytes() const;
        dynarray<std::byte> GetTeamBytes() const;
        dynarray<std::byte> GetActiveBoxBytes() const;
        dynarray<dynarray<std::byte>> GetBoxBankBytes(int) const;

    private:
        shared_dynarray<const std::shared_ptr<const Box>> _inactiveBoxes;
        std::shared_ptr<const Box> _activeBox;
        std::shared_ptr<const Box> _team;
        std::shared_ptr<const Box> _daycare;
        std::byte _currentBox;
        std::array<std::byte, 2> _allBoxesChecksums;
        std::shared_ptr<Mappers> _mappers;

        std::optional<int> ActiveBoxIndex() const;
        int GetPcBoxCount() const;
        std::shared_ptr<const Box> AtIndex(int) const;
        bool IsTeam(int index) const;
        bool IsDaycare(int index) const;
        bool IsActiveBox(int index) const;
        std::shared_ptr<const Box> GetActiveBox() const;
        std::shared_ptr<const Box> GetBox(int) const;

    public:
        // should be private, but std::make_shared
        StorageSystem(decltype(_inactiveBoxes),
            decltype(_activeBox),
            decltype(_team),
            decltype(_daycare),
            decltype(_currentBox),
            decltype(_allBoxesChecksums),
            decltype(_mappers));
    };
    sol::usertype<StorageSystem> MakeStorageSystemUsertype();
}

#endif
