#ifndef MONLIB_GAMES_GEN1_LUA_PK1BINARYFORMAT_H
#define MONLIB_GAMES_GEN1_LUA_PK1BINARYFORMAT_H

#include "Monlib/Games/Gen1/Lua/BinaryData.h"
#include "Monlib/Games/Gen1/Lua/BinaryFormats.h"
#include "Monlib/Games/Gen1/Lua/ConcatBinaryFormat.h"
#include "Monlib/Games/Gen1/Lua/Monster.h"
#include "Monlib/Games/Gen1/Lua/MonsterStorageType.h"

namespace Monlib::Games::Gen1::Lua {
    class Pk1BinaryFormat : public BinaryFormat {
    public:
        virtual LuaBinaryData Serialize(const Monster& m) const override {
            auto result = ConcatBinaryFormat(MonsterStorageType::Team).Serialize(m);
            result.insert(0, "\x01");
            result.insert(2, "\xFF");
            result.insert(3, 1, result[1]);
            return result;
        }
        virtual std::shared_ptr<const Monster> Deserialize(
            const LuaBinaryData& d, std::shared_ptr<Mappers> m) const override {
            LuaBinaryData trimmed = d;
            trimmed.erase(2, 1);
            trimmed.erase(0, 1);
            return ConcatBinaryFormat(MonsterStorageType::Team)
                .Deserialize(trimmed, m);
        }
    };
}

#endif
