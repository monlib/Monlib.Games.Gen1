#ifndef MONLIB_GAMES_GEN1_LUAOPEN_H
#define MONLIB_GAMES_GEN1_LUAOPEN_H

#include "lua.hpp"

extern "C" int luaopen_monlib_games_gen1(lua_State*) noexcept;

#endif
