#ifndef MONLIB_GAMES_GEN1_SOLWRAP_H
#define MONLIB_GAMES_GEN1_SOLWRAP_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#include <sol.hpp>
#pragma GCC diagnostic pop

namespace Monlib::Games::Gen1 {
    inline sol::variadic_results LuaError(sol::this_state lua, const char* msg) {
        return sol::variadic_results{sol::nil, sol::object(lua, sol::in_place, msg)};
    }
}

#endif
