#ifndef MONLIB_GAMES_GEN1_LUA_MONSTER_H
#define MONLIB_GAMES_GEN1_LUA_MONSTER_H

#include <memory>

#include "Monlib/Games/Gen1/Bytes/Monster.h"
#include "Monlib/Games/Gen1/Lua/Mappers.h"
#include "Monlib/Games/Gen1/Lua/MonsterStorageType.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"

namespace Monlib::Games::Gen1::Lua {
    class BinaryFormatList;
    class Monster {
    public:
        Monster(Bytes::Monster,
            shared_dynarray<const std::byte> nickname,
            shared_dynarray<const std::byte> otName,
            std::shared_ptr<Mappers> mappers);
        int GetSpecies() const;
        int GetExperience() const;
        int GetLevel() const;
        std::string GetNickname() const;
        std::string GetOtName() const;
        int GetOtId() const;
        sol::object EditableProperties(sol::this_state) const;
        sol::variadic_results ModifiedWith(
            sol::this_state, sol::table, sol::optional<bool>) const;
        sol::table GetBinaryFormats(sol::this_state) const;
        sol::variadic_results SerializeTo(sol::this_state, std::string format) const;

        MonsterStorageType GetStorageType() const;

        Bytes::Monster GetMonsterBytes() const;
        shared_dynarray<const std::byte> GetNicknameBytes() const;
        shared_dynarray<const std::byte> GetOtNameBytes() const;

        int SumOfBytes() const;

    private:
        Bytes::Monster _monster;
        shared_dynarray<const std::byte> _nickname;
        shared_dynarray<const std::byte> _otName;
        std::shared_ptr<Mappers> _mappers;

        bool IsTeam() const;
        bool IsBox() const;
        const BinaryFormatList& GetFormatList() const;
    };

    sol::usertype<Monster> MakeMonsterUsertype();
}

#endif
