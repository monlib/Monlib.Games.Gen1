#ifndef MONLIB_GAMES_GEN1_LUA_MAPPERS_H
#define MONLIB_GAMES_GEN1_LUA_MAPPERS_H

#include "Monlib/Games/Gen1/SpeciesMapper.h"

namespace Monlib::Games::Gen1::Lua {
    struct Mappers {
        SpeciesMapper species;
    };
}

#endif
