#ifndef MONLIB_GAMES_GEN1_LUA_MONSTERSTORAGETYPE_H
#define MONLIB_GAMES_GEN1_LUA_MONSTERSTORAGETYPE_H

#include <gsl/gsl>

#include "Monlib/Games/Gen1/Bytes/Monster.h"

namespace Monlib::Games::Gen1::Lua {
    enum class MonsterStorageType {
        Box,
        Team,
    };

    constexpr int MonsterSize(MonsterStorageType t) {
        switch (t) {
        case MonsterStorageType::Box: return Bytes::BoxedMonsterSize;
        case MonsterStorageType::Team: return Bytes::TeamMonsterSize;
        default: Expects(false);
        }
    }
}

#endif
