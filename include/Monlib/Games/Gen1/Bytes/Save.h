#ifndef MONLIB_GAMES_GEN1_BYTES_SAVE_H
#define MONLIB_GAMES_GEN1_BYTES_SAVE_H

#include <cstddef>
#include <memory>
#include <optional>
#include <string>

#include "Monlib/Games/Gen1/Bytes/VersionDifferences.h"
#include "Monlib/Games/Gen1/Dynarray.h"

namespace Monlib::Games::Gen1::Bytes {
    class Save {
    public:
        static constexpr int size = 0x8000;
        using shared_bytes = shared_dynarray<const std::byte>;

        Save(shared_dynarray<const std::byte>, Region);
        shared_bytes GetBank0() const;
        shared_bytes GetBank1PreTrash() const;
        shared_bytes GetTrainerNameBytes() const;
        shared_bytes GetSpriteData() const;
        shared_bytes GetMainDataWoDaycare() const;
        shared_bytes GetTeamBytes() const;
        shared_bytes GetDaycareBytes() const;
        shared_bytes GetCurrentBoxBytes() const;
        std::byte GetCurrentBoxIndex() const;
        shared_bytes GetUncheckedBoxBytes(int) const;
        shared_bytes GetChecksummedRegion() const;
        std::byte GetTilesetByte() const;
        std::byte GetChecksumByte() const;
        std::byte GetBoxBankChecksumByte(int) const;
        std::byte GetBoxChecksumByte(int) const;
        shared_bytes GetBank1PostTrash() const;
        shared_bytes GetBoxBankTrash(int) const;

        Region GetRegion() const { return _region; }
        shared_dynarray<const std::byte> GetData() const { return _data; }

    protected:
        shared_dynarray<const std::byte> _data;
        Region _region;
    };

    int GetCurrentBoxOffset(Region);

    std::optional<Save> LoadSave(std::string filename);
}

#endif
