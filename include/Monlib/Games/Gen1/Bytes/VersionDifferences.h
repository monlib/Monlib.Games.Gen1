#ifndef MONLIB_GAMES_GEN1_BYTES_VERSIONDIFFERENCES_H
#define MONLIB_GAMES_GEN1_BYTES_VERSIONDIFFERENCES_H

namespace Monlib::Games::Gen1::Bytes {
    enum class Region { Japan, Usa };

    constexpr int GetNicknameLength(Region r) { return r == Region::Japan ? 6 : 11; }
    constexpr int GetOtNameLength(Region r) { return r == Region::Japan ? 6 : 11; }
    constexpr int GetBoxCapacity(Region r) { return r == Region::Japan ? 30 : 20; }
    constexpr int GetBoxCount(Region r) { return r == Region::Japan ? 8 : 12; }
}

#endif
