#ifndef MONLIB_GAMES_GEN1_BYTES_MONSTER_H
#define MONLIB_GAMES_GEN1_BYTES_MONSTER_H

#include <array>
#include <cstddef>
#include <optional>

#include "Monlib/Games/Gen1/Dynarray.h"

namespace Monlib::Games::Gen1::Bytes {
    constexpr auto BoxedMonsterSize = 33;
    constexpr auto TeamMonsterSize = 44;
    constexpr auto MonsterSize(bool isTeam) {
        return isTeam ? TeamMonsterSize : BoxedMonsterSize;
    }

    enum class Stat { Hp = 0, Attack = 1, Defense = 2, Speed = 3, Special = 4 };

    class MutableMonster;
    class Monster {
    public:
        using byte = std::byte;
        template <class T, int N>
        using array = std::array<T, N>;

        Monster(shared_dynarray<const byte>);

        byte GetSpecies() const;
        array<byte, 2> GetCurrentHP() const;
        byte GetPcLevel() const;
        byte GetStatus() const;
        array<byte, 2> GetTypes() const;
        byte GetCatchRate() const;
        array<byte, 4> GetMoves() const;
        array<byte, 2> GetOtId() const;
        array<byte, 3> GetExp() const;
        array<byte, 2> GetStatEv(Stat) const;
        array<byte, 2> GetIvData() const;
        array<byte, 4> GetPpValues() const;

        std::optional<byte> GetTeamLevel() const;
        std::optional<array<byte, 2>> GetMaxHp() const;

        // GetTeamStat(Stat::Hp) == GetMaxHp();
        std::optional<array<byte, 2>> GetTeamStat(Stat) const;
        MutableMonster Copy() const;

        shared_dynarray<const byte> GetData() const;

    protected:
        shared_dynarray<const byte> _data;
    };

    class MutableMonster : public Monster {
    public:
        // Aliases to remove the std:: spam.
        using byte = std::byte;
        template <class T, int N>
        using array = std::array<T, N>;

        MutableMonster(shared_dynarray<byte>);
        void SetOtId(array<byte, 2>) const;
        void OverwriteWith(const Monster&) const;
        shared_dynarray<byte> GetMutableData() const;

    private:
        shared_dynarray<byte> _mutableData;
    };
}

#endif
