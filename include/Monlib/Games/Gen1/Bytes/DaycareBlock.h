#ifndef MONLIB_GAMES_GEN1_BYTES_DAYCAREBLOCK_H
#define MONLIB_GAMES_GEN1_BYTES_DAYCAREBLOCK_H
#include <cstddef>

#include "Monlib/Games/Gen1/Bytes/Monster.h"
#include "Monlib/Games/Gen1/Bytes/VersionDifferences.h"
#include "Monlib/Games/Gen1/Dynarray.h"

namespace Monlib::Games::Gen1::Bytes {
    class DaycareBlock {
    public:
        DaycareBlock(shared_dynarray<const std::byte>, Region);
        const std::byte& GetOccupied() const;
        shared_dynarray<const std::byte> GetNickname() const;
        shared_dynarray<const std::byte> GetOtName() const;
        shared_dynarray<const std::byte> GetMonster() const;

        shared_dynarray<const std::byte> GetData() const;
        Region GetRegion() const;

    protected:
        shared_dynarray<const std::byte> _data;
        Region _region;
    };

    constexpr int DaycareBlockSize(Region region) {
        auto capacitySize = 1;
        auto stringSize = GetNicknameLength(region);
        auto monSize = BoxedMonsterSize;
        return capacitySize + 2 * stringSize + monSize;
    }
};

#endif
