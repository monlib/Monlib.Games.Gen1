#ifndef MONLIB_GAMES_GEN1_BYTES_MONSTERLIST_H
#define MONLIB_GAMES_GEN1_BYTES_MONSTERLIST_H

#include <cstddef>

#include "Monlib/Games/Gen1/Bytes/Monster.h"
#include "Monlib/Games/Gen1/Bytes/VersionDifferences.h"
#include "Monlib/Games/Gen1/Dynarray.h"

namespace Monlib::Games::Gen1::Bytes {
    class MonsterList {
    public:
        using shared_bytes = shared_dynarray<const std::byte>;

        MonsterList(shared_bytes, int, Region);
        const std::byte& GetUsedEntries() const;
        shared_bytes GetSpeciesList() const;
        shared_bytes GetMonster(int) const;
        shared_bytes GetMonsters() const;
        shared_bytes GetOtName(int) const;
        shared_bytes GetOtNames() const;
        shared_bytes GetNickname(int) const;
        shared_bytes GetNicknames() const;

        shared_bytes GetData() const;
        int GetCapacity() const;
        Region GetRegion() const;
        bool IsTeam() const;

    protected:
        shared_bytes _data;
        int _capacity;
        Region _region;
    };

    constexpr auto TeamCapacity = 6;
    constexpr auto SpeciesListTerminator = std::byte{0xFF};

    int MonsterListSize(Region region, int capacity);
}

#endif
