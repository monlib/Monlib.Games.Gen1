Monster
=======

*A single monster which can be inspected and edited*

.. py:class:: Monster

    Allows you to inspect, change and serialize monsters. These objects satisfy
    the `Monster concept from Monlib.Core <https://monlibcore.readthedocs.io/en/latest/Objects.html#Monster>`_.

    .. py:attribute:: editableProperties

        A sequence of properties which can be changed using :py:meth:`modifiedWith`.

        :rtype: table
        :returns: A sequence of strings

    .. py:method:: modifiedWith(changes[, ignoreUnknown=False])

        Returns a copy of this monster where each property in ``changes`` has its
        value in ``changes``.

        Unless ``ignoreUnknown`` is ``True``, any unkown key in ``changes`` results
        in a failure.

        :rtype: Monster or (nil, string)
        :returns: Either a modified monster or nil and an error message.

        Example:

        .. code-block:: lua

            local changes = { originalTrainerId = 5234 }
            local newMonster, message = monster:modifiedWith(changes)
            if newMonster then
                assert(newMonster.originalTrainerId == 5234)
            else
                print(message)
            end

    .. py:attribute:: binaryFormats

        This property indicates which formats this monster can be serialized to.

        :rtype: table
        :returns: A sequence of strings (BinaryFormatTags)

    .. py:method:: serializeTo(format)

        :param string format: The format to serialize to. Must be in :py:attr:`binaryFormats`.
        :rtype: string or (nil, string)
        :returns: Either this monster serialized as a string or nil and an
            error message.

    .. py:attribute:: species

        The ID of this monster's species.

        :rtype: int

    .. py:attribute:: experience

        :rtype: int

    .. py:attribute:: level

        This attribute returns the level from the team data block or, if that
        doesn't exist, the one from the core data. This value might be wrong if
        experience and level somehow got out of sync.

        :rtype: int

    .. py:attribute:: nickname

        :rtype: string

    .. py:attribute:: originalTrainerName

        :rtype: string

    .. py:attribute:: originalTrainerId

        :rtype: int
