luaopen
=======

*The function called by Lua to load this module*

.. cpp:function:: int luaopen_monlib_games_gen1(lua_State*)

This function is used by lua to load the module. You most likely use it only
when calling ``require('monlib.games.gen1`)``. All API methods are returned as
a table and not placed in the global environment, i.e. you must assign the
result of calling ``require('monlib.games.gen1')`` to some variable to use
this package.

The global environment is modified by this function: several userdata are
added with keys beginning with ``sol.Monlib::Games::Gen1::`` and ending
with ``.♻``.

.. warning::
    This function may call :c:func:`abort` since any errors it encounters
    indicate a damaged lua state (returning to which might cause undefined
    behvaior)

.. note::
    This is a C function and thus not directly callable from Lua.

Example
-------

.. code-block:: lua

    local gen1 = require('monlib.games.gen1')
