How To Read This Reference
==========================

Because this documentation is written using tools that weren't made for Lua,
there are some oddities that need to be explained.

Of Types and Classes
--------------------

Those familiar with Lua will know that there are only 7 types: nil, number,
string, boolean, table, function, thread, and userdata. These types do not
convey much information about what can be done with an object (especially
table and userdata), so this documentation uses "classes". Since classes
do not exist in Lua, there are a few important points to note here:

- There may or may not be a metatable which all instances of this type share.
- There may or may not be an entry in the module table with the class's name.
- Member functions must always be obtained from an object.

So, when this documentation refers to a "type", it means "a table or userdata
which provides the methods and attributes that are described in the section $type".

The Monlib.Core API
-------------------

Throughout these documents, you will encounter references to the
`Monlib.Core API <https://monlibcore.readthedocs.io>`_. This package tries to
expose most of its functionality through that API, since it allows for making
tools compatible with multiple generations of games. This documentation
should be readable on its own, but you should still check it out if you want to
interoperate with saves from different generations. If you are only relying on
the behavior described in that API, you can likely reuse your code for other
generations/games! 
