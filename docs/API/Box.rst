Box
===

*A list of monsters*

.. py:class:: Box

    A Box is mainly an array of monsters with some additional functionality.
    These objects satisfy the
    `Box concept from Monlib.Core <https://monlibcore.readthedocs.io/en/latest/Objects.html#Box>`_.

    .. py:method:: __index(i)

        Gets the Monster at the specified index. For i ∉ [1; capacity] this
        function always returns nil.

        :param int i: 1-based index into the box
        :rtype: Monster or nil

    .. py:attribute:: capacity

        Returns this box's *capacity* (e.g. 6 for the team).

        :rtype: int

    .. py:attribute:: name

        A human readable string to describe this box. Examples include "Box 5" and "Daycare".

        :rtype: string

    .. py:attribute:: acceptedBinaryFormats

        This attribute indicates which monsters can be inserted into this box.
        Any monster that is passed as an argument to :py:meth:`insert` must have
        at least one element of this sequence in its :py:attr:`~Monster.binaryFormats`.
        This restiction might force you to "migrate" a Monster from one
        representation to another before inserting it. There is not yet a
        standard way of doing so.

        :rtype: table
        :returns: A sequence of strings (BinaryFormatTags)

        .. note::
            ``monster`` can be any object that satisfies the `Monlib.Core
            Monster concept <https://monlibcore.readthedocs.io/en/latest/Objects.html#Monster>`_
            and the requirements above.
