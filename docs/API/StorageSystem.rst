StorageSystem
=============

*A collection of Boxes*

.. py:class:: StorageSystem

    Provides access to a save's monsters through boxes. These objects
    satisfy the `MonsterStorage concept from Monlib.Core <https://monlibcore.readthedocs.io/en/latest/Objects.html#MonsterStorage>`_.

    .. py:method:: __len()

        :returns: The number of boxes accessible via :py:meth:`__index`
        :rtype: int

    .. py:method:: __index(i)

        This method allows you to treat a StorageSystem like an ordinary sequence of
        :py:class:`Box` objects. Which boxes are accessible and their order depends
        on where you got this object from.

        :param int i: 1-based index
        :returns: The box at that index if i ∈ [1; #self], otherwise nil.
        :rtype: Box or nil

    .. py:attribute:: team

        :rtype: Box

    .. py:attribute:: daycare

        :rtype: Box

    .. py:attribute:: pcStorage

        :returns: A sequence containing all boxes of the PC storage sytem
        :rtype: sequence(Box)

    .. py:method:: withBoxAt(box, keys...)

        This method returns a copy of the storage system with the specified
        box at ``keys``. ``keys`` are one or more arguments which identify where
        to place the box (e.g. ("pcStorage", 5,) would update result.pcStorage[5]).
        Other sequences of keys which refer to the same box (e.g. ("team",) and (13,))
        are also changed. All other boxes remain the same as in ``self``.

        :param Box box: the box to insert
        :param object... keys: One or more keys identifying where to insert the box
        :rtype: StorageSystem or (nil, string)
        :returns: A StorageSystem with ``box`` at ``keys`` or nil and an error message
