Save
====

*The high level view of a save file*

.. py:function:: Save.new(filename, speciesMapping)

    Opens the given file as a save file.

    :param string filename: Filename
    :param table speciesMapping:
        A table which maps species index numbers to the desired output
        of functions like :py:meth:`Monster.species`.
    :rtype: Save or nil
    :returns: Either the loaded save or nil on failure.

    .. warning:: This method is *extremely* likely to change prior to 1.0.0.

.. py:class:: Save

    Provides access to an entire save file. These objects
    satisfy the `Save concept from Monlib.Core <https://monlibcore.readthedocs.io/en/latest/Objects.html#Save>`_.


    .. py:attribute:: monsterStorage

        Provides access to all means of storing monsters in the save. The returned
        StorageSystem can be indexed to access the team, daycare, and PC.

        :rtype: StorageSystem

    .. py:method:: serializeToString()

        :returns: This save's raw bytes
        :rtype: string

    .. py:method:: serializeToFile(file)

        Writes this save's raw bytes to the file ``file``.

        :param file: A file-like object (see relevant Monlib.Core docs)
        :returns: nil
        :raises: If writing failed

    .. py:method:: withMonsterStorage(storage)

        Returns a copy of this Save with :py:attr:`monsterStorage` replaced with ``storage``.

        :param storage StorageSystem: The new monster storage
        :rtype: Save or (nil, string)
        :returns: A save or nil and an error message
