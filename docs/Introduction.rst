Introduction
============

Monlib.Games.Gen1 implements the `Monlib.Core API`_ for save files of
Red, Green, Blue, and Yellow Edition from all regions. It is compatible with
both 32 bit and 64 bit Lua 5.3 and provides easy, structured access to a
save's data.

To get started with editing a save file, have a look at :py:meth:`Save.new`.

.. note::
    It's useful to have at least an overview of how the `Monlib.Core API`_ is laid
    out, since this package implements some subset of it alongside its own
    extensions.

Example
-------

.. TODO: Maybe literalinclude this example and somehow test it?

.. code-block:: lua

    local gen1 = require('monlib.games.gen1')

    local save = gen1.Save.new('mysave.sav', speciesMapper) -- See note
    print(save.monsterStorage.pcStorage[2][5].level) --> 23
    local mon = save.monsterStorage.team[1]
    local newMon = mon.modifiedWith({nickname = "notMEW"})
    print(mon.nickname) --> "MEW"
    print(newMon.nickname) --> "notMEW"

.. _`Monlib.Core API`: https://monlibcore.readthedocs.org
