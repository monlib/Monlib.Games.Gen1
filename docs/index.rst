Monlib.Games.Gen1
=================

*Edit first generation save files through Lua*

Monlib.Games.Gen1 implements the `Monlib.Core API`_ for the save files of first
generation Pokemon games.

.. note::
    Never use cheated saves to ruin other people's fun, especially online.

.. note::
    This project is not in any way affiliated with Nintendo or The Pokemon Company.


.. toctree::
    :maxdepth: 2
    :caption: Concepts
    
    Introduction

.. toctree::
    :maxdepth: 2
    :caption: API Reference

    API/HowToReadThis
    API/LuaOpen
    API/Save
    API/StorageSystem
    API/Box
    API/Monster


.. _`Monlib.Core API`: https://monlibcore.readthedocs.org
