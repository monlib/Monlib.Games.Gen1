#!/bin/bash

set -e

BUILD_OPTS="-o lua:forModule=False -o Monlib.Games.Gen1:shared=False -s build_type=Debug"
BUILD_DIR=build
REFERENCE="Monlib.Games.Gen1/someversion@ahoischen/testing"

conan install --install-folder "${BUILD_DIR}" --build missing ${BUILD_OPTS} .
conan build --build-folder "${BUILD_DIR}" .
conan export-pkg --build-folder "${BUILD_DIR}" --force . "${REFERENCE}"
conan test --build missing ${BUILD_OPTS} test/ "${REFERENCE}"
