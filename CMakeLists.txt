cmake_minimum_required(VERSION 3.10)
project(Monlib.Games.Gen1 CXX)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} \
    -DGSL_THROW_ON_CONTRACT_VIOLATION \
    -Waggressive-loop-optimizations \
    -Wall \
    -Wcast-qual \
    -Wchkp \
    -Wduplicated-branches \
    -Wduplicated-cond \
    -Wextra \
    -Wfloat-equal \
    -Wformat-signedness \
    -Wformat=2 \
    -Wframe-larger-than=4096 \
    -Winit-self \
    -Wlogical-op \
    -Wnon-virtual-dtor \
    -Wnonnull \
    -Wnull-dereference \
    -Wold-style-cast \
    -Woverloaded-virtual \
    -Wrestrict \
    -Wshadow \
    -Wsign-compare \
    -Wsign-promo \
    -Wtrampolines \
    -Wuninitialized \
    ")
if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
endif()
include_directories(include)
add_library(mon-games-gen1
    src/Bytes/DaycareBlock.cpp
    src/Bytes/Monster.cpp
    src/Bytes/MonsterList.cpp
    src/Bytes/Save.cpp
    src/Interpret/Box.cpp
    src/Interpret/Daycare.cpp
    src/Interpret/Encodings.cpp
    src/Interpret/Monster.cpp
    src/Interpret/Save.cpp
    src/Lua/BinaryFormatLists.cpp
    src/Lua/BinaryFormats.cpp
    src/Lua/Box.cpp
    src/Lua/ConcatBinaryFormat.cpp
    src/Lua/LuaOpen.cpp
    src/Lua/Monster.cpp
    src/Lua/Save.cpp
    src/Lua/StorageSystem.cpp
)
target_link_libraries(mon-games-gen1 ${CONAN_LIBS})
