#include "catch.hpp"

#include "Monlib/Games/Gen1/Bytes/Save.h"

using Monlib::Games::Gen1::Bytes::LoadSave;

template <size_t len>
constexpr std::array<std::byte, len> ToByteArr(std::array<int, len> in) {
    using std::begin, std::end;
    std::array<std::byte, len> out{};
    std::transform(
        begin(in), end(in), begin(out), [](int i) { return std::byte(i); });
    return out;
}

SCENARIO("A Save provides access to a save") {
    GIVEN("A slightly glitched Pokémon Red Save") {
        auto save = LoadSave("./data/slightlyhacked.sav").value();
        WHEN("The trainer's name is requested") {
            auto actualBytes = save.GetTrainerNameBytes();
            THEN("It is correct") {
                auto expectedBytes = ToByteArr(std::array{0x91, 0x84, 0x83, 0x50});
                using std::begin, std::end;
                REQUIRE(std::equal(
                    begin(expectedBytes), end(expectedBytes), begin(actualBytes)));
            }
        }

        WHEN("The team's bytes are requested") {
            auto actual = save.GetTeamBytes();
            THEN("They are correct") {
                auto expected = ToByteArr(std::array{0x04, 0x24, 0x2E, 0x22, 0x02});
                using std::begin, std::end;
                REQUIRE(std::equal(begin(expected), end(expected), begin(actual)));
            }
        }
    }
}
