#include <iostream>

#include "Monlib/Games/Gen1/Lua/LuaOpen.h"
#include "Monlib/Games/Gen1/Lua/SolWrap.h"

int main(int, char* []) {
    sol::state lua{};
    lua.open_libraries(sol::lib::base,
        sol::lib::coroutine,
        sol::lib::debug,
        sol::lib::io,
        sol::lib::math,
        sol::lib::os,
        sol::lib::package,
        sol::lib::string,
        sol::lib::table,
        sol::lib::utf8);
    lua.require("monlib.games.gen1", ::luaopen_monlib_games_gen1, false);
    int exitCode = lua.script_file("main.lua");
    std::cout << std::endl;
    return exitCode;
}
