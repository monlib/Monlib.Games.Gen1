--[[
    This module provides extensions for objgraphtest, e.g. for allowing nil-checks, etc.
]]
local objgraphexts = {}

local luaunit = require("luaunit")
local objgraphtest = require("objgraphtest")



objgraphexts.LengthProperty = {}
objgraphexts.LengthProperty.__index = objgraphexts.LengthProperty

function objgraphexts.LengthProperty.new()
    return setmetatable({}, objgraphexts.LengthProperty)
end

function objgraphexts.LengthProperty:getTests(value, getActual, prefix, hist)
    return objgraphtest.getPropTests(
        value,
        function () return #getActual() end,
        prefix .. "-" .. "[__len]",
        hist)
end



objgraphexts.NilExpectation = {}
objgraphexts.NilExpectation.__index = objgraphexts.NilExpectation

function objgraphexts.NilExpectation.new()
    return setmetatable({}, objgraphexts.NilExpectation)
end

function objgraphexts.NilExpectation:getTests(getProp, testName)
    return { [testName] = function () luaunit.assertIsNil(getProp()) end }
end



objgraphexts.SameAsReference = {}
objgraphexts.SameAsReference.__index = objgraphexts.SameAsReference

--[[
    Uses args to index t recursively:
    getInTable(t, "foo", "bar", 1) == t["foo"]["bar"][1]
]]
local function getInTable(t, ...)
    local arg = table.pack(...)
    local newT = t[arg[1]]
    if arg.n > 1 then
        return getInTable(newT, table.unpack(arg, 2))
    else
        return newT
    end
end

function objgraphexts.SameAsReference.new(...)
    return setmetatable({_keys = table.pack(...)}, objgraphexts.SameAsReference)
end

function objgraphexts.SameAsReference:getTests(getProp, testName, hist)
    local aliasedVal = getInTable(hist[1].expected, table.unpack(self._keys))
    if aliasedVal ~= nil then
        return objgraphtest.getPropTests(aliasedVal, getProp, testName, hist)
    else
        return {}
    end
end


--[[
    Foreach Key. Example:
        [foreach(1, 12)] = function (i) return sameAs("monsterStorage", i) end
]]
objgraphexts.ExpectForeach = {}
objgraphexts.ExpectForeach.__index = objgraphexts.ExpectForeach

function objgraphexts.ExpectForeach.new(first, last)
    return setmetatable({
            _first = first,
            _last = last,
        },
        objgraphexts.ExpectForeach)
end

function objgraphexts.ExpectForeach:getTests(value, getActual, prefix, hist)
    local tests = {}
    for i = self._first, self._last do
        local testName = prefix .. "-" .. i
        local getProp = function () return getActual()[i] end
        local actualValue = value(i)
        local generatedTests = objgraphtest.getPropTests(actualValue, getProp, testName, hist) 
        for testName, test in pairs(generatedTests) do
            tests[testName] = test
        end
    end
    return tests
end



objgraphexts.Behavior = {}
objgraphexts.Behavior.__index = objgraphexts.Behavior

function objgraphexts.Behavior.new()
    return setmetatable({}, objgraphexts.Behavior)
end

function objgraphexts.Behavior:getTests(value, getActual, prefix, hist)
    local hist, prevState = hist:pop()
    local testName = prevState.prefix .. "$Behaviour#" .. string.gsub(tostring(self), "table: ", "")
    return value:getTests(prevState.expected, prevState.getActual, testName, hist)
end



return objgraphexts
