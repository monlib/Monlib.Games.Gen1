local SerializationBehavior = {}
SerializationBehavior.__index = SerializationBehavior

local luaunit = require("luaunit")

function SerializationBehavior.new(format, filename)
    return setmetatable({_format = format, _filename = filename}, SerializationBehavior)
end

local function assertHasFormat(mon, format)
    local isPresent = false
    for k, v in pairs(mon.binaryFormats) do
        if v == format then
            isPresent = true
        end
    end
    luaunit.assertTrue(isPresent)
end

local function readfile(filename)
    local f = io.open(filename, "rb")
    if f then
        local contents = f:read("a")
        f:close()
        return contents
    end
end

local function readable(bin)
    local fmt = string.rep("%02X ", #bin)
    return fmt:format(bin:byte(1, -1))
end

local function assertSerializesCorrectly(mon, format, filename)
    local serialized = assert(mon:serializeTo(format))
    local expected = readfile(filename)
    luaunit.assertEquals(readable(serialized), readable(expected))
end

function SerializationBehavior:getTests(desc, getMon, testName, hist)
    local function test(f) return function () f(getMon(), self._format, self._filename) end end
    return {
        [testName .. "-hasFormat"] = test(assertHasFormat),
        [testName .. "-serializesCorrectly"] = test(assertSerializesCorrectly),
    }
end

return SerializationBehavior
