local MonsterStorageInsertBoxBehavior = {}
MonsterStorageInsertBoxBehavior.__index = MonsterStorageInsertBoxBehavior

local luaunit = require("luaunit")

function MonsterStorageInsertBoxBehavior.new(expectNil, lengthProperty)
    assert(expectNil)
    assert(lengthProperty)
    return setmetatable({
        _expectNil = expectNil,
        _lengthProperty = lengthProperty,
    }, MonsterStorageInsertBoxBehavior)
end

local function getLen(desc, expectNil)
    for i = 1, 1000 do
        if desc[i] == expectNil then return i - 1 end
    end
    assert(false, "Cannot find first nil entry!")
end

local assertions = {}

function assertions.canInsertSelf(storage, index)
    luaunit.assertNotNil(storage:withBoxAt(storage[index], index))
end

function MonsterStorageInsertBoxBehavior:getTests(desc, getBox, testName, hist)
    local tests = {}
    local function addTest(name, ...)
        args = { n = select("#", ...), ...}
        local testName = testName .. "-test" .. name:sub(1, 1):upper() .. name:sub(2)
        tests[testName] = function ()
            assertions[name](getBox(), table.unpack(args, 1, args.n))
        end
    end
    local len = getLen(desc, self._expectNil)
    if  1 <= len then addTest("canInsertSelf", 1) end
    return tests
end

return MonsterStorageInsertBoxBehavior