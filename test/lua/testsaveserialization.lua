local SaveSerializationBehavior = {}
SaveSerializationBehavior.__index = SaveSerializationBehavior

local luaunit = require("luaunit")

function SaveSerializationBehavior.new(filename)
    return setmetatable({ _filename = filename }, SaveSerializationBehavior)
end

local function assertSerializesToString(save, expected)
    local serialized = assert(save:serializeToString())
    luaunit.assertEquals(serialized, expected)
end

local function assertSerializesToFile(save, expected)
    local f = io.tmpfile()
    if f then
        local serialized, error = pcall(save.serializeToFile, save, f)
        if serialized then
            local didTheTest = pcall(function ()
                assert(f:seek("set", 0))
                local actual = assert(f:read("a"))
                luaunit.assertEquals(actual, expected)
            end)
            if not didTheTest then
                print("Could not test SerializesToFile!")
            end
        end
        f:close()
        assert(serialized, error)
    else
        print("Could not test SerializesToFile!")
    end
end

function SaveSerializationBehavior:getTests(desc, getSav, testName, hist)
    expected = self:_loadExpected()
    local function test(f) return function () f(getSav(), expected) end end
    return {
        [testName .. "-serializesToString"] = test(assertSerializesToString),
        [testName .. "-serializesToFile"] = test(assertSerializesToFile),
    }
end

function SaveSerializationBehavior:_loadExpected()
    local f = io.open(self._filename, "rb")
    if f then
        local contents = f:read("a")
        f:close()
        return contents
    end
end

return SaveSerializationBehavior
