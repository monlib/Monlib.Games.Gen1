local MonsterEditingBehavior = {}
MonsterEditingBehavior.__index = MonsterEditingBehavior

local luaunit = require("luaunit")

function MonsterEditingBehavior.new()
    return setmetatable({}, MonsterEditingBehavior)
end

local function assertEditablePropertiesIsComplete(mon)
    luaunit.assertItemsEquals(mon.editableProperties, { "originalTrainerId" })
end

local function assertModifiedWithEmptyIsId(srcMon)
    local destMon = assert(srcMon:modifiedWith({}))
    luaunit.assertEquals(destMon.species, srcMon.species)
end

local function assertModifiedWithUnknownKeyIsError(srcMon)
    local invalidChanges = { [{}] = 1 }
    local result, msg = srcMon:modifiedWith(invalidChanges, false)
    luaunit.assertError(assert, result, msg)
end

local function assertOriginalTrainerIdIsModifiable(srcMon)
    local destMon = assert(srcMon:modifiedWith({ originalTrainerId = 0xcafe }))
    luaunit.assertEquals(destMon.originalTrainerId, 0xcafe)
end

function MonsterEditingBehavior:getTests(desc, getMon, testName, hist)
    local function test(f) return function () f(getMon()) end end
    return {
        [testName .. "-editableProperties"] = test(assertEditablePropertiesIsComplete),
        [testName .. "-modifiedWithNothing"] = test(assertModifiedWithEmptyIsId),
        [testName .. "-modifiedWithInvalidKey"] = test(assertModifiedWithUnknownKeyIsError),
        [testName .. "-modifiedWithOriginalTrainerId"] = test(assertOriginalTrainerIdIsModifiable),
    }
end

return MonsterEditingBehavior
