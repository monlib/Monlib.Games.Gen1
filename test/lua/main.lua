local monlib = { games = { gen1 = require("monlib.games.gen1") } }
local luaunit = require("luaunit")
local objgraphexts = require("objgraphexts")
local objgraphtest = require("objgraphtest")
local MonsterEditingBehavior = require("testmonsteredit")
local BoxInsertingBehavior = require("testboxinsert")
local SerializationBehavior = require("testserialization")
local SaveSerializationBehavior = require("testsaveserialization")
local MonsterStorageInsertBoxBehavior = require("testmonsterstorageinsertbox")

local function makeTestEnv()
    local env = {}
    env.lengthProperty = objgraphexts.LengthProperty.new()
    env.expectNil = objgraphexts.NilExpectation.new()
    env.sameAs = objgraphexts.SameAsReference.new
    env.foreach = objgraphexts.ExpectForeach.new
    env.newBehavior = objgraphexts.Behavior.new
    env.monsterEditing = MonsterEditingBehavior.new()
    env.boxInserting = BoxInsertingBehavior.new(env.expectNil, env.lengthProperty)
    env.serializationTo = SerializationBehavior.new
    env.saveSerializing = SaveSerializationBehavior.new
    env.monsterStorageInsertBox = MonsterStorageInsertBoxBehavior.new(env.expectNil, env.lengthProperty)
    return env
end

local function loadObjGraphTests(filename)
    env = makeTestEnv()
    local chunk = assert(loadfile(filename, "t", env))
    assert(pcall(chunk))
    local testTable = {}
    objgraphtest.addTests(
        testTable,
        "test",
        function () return monlib.games.gen1.Save.new(env.filename, require("speciesids")) end,
        env.expectedObjects)
    _ENV["Testing object graph of \"" .. env.friendlyname .. "\""] = testTable
end

loadObjGraphTests("data/slightlyhacked.lua")

TestSaveCtor = {}
function TestSaveCtor.testExists()
    luaunit.assertIsTable(monlib.games.gen1.Save)
    luaunit.assertIsFunction(monlib.games.gen1.Save.new)
end

return luaunit.LuaUnit.run()
