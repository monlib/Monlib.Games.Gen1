local BoxInsertingBehavior = {}
BoxInsertingBehavior.__index = BoxInsertingBehavior

local luaunit = require("luaunit")

function BoxInsertingBehavior.new(expectNil, lengthProperty)
    assert(expectNil)
    assert(lengthProperty)
    return setmetatable({
        _expectNil = expectNil,
        _lengthProperty = lengthProperty
    }, BoxInsertingBehavior)
end

function BoxInsertingBehavior:_getLen(desc)
    for i = 1, 1000 do
        if desc[i] == self._expectNil then return i - 1 end
    end
    assert(false, "Cannot find first nil entry!")
end

function BoxInsertingBehavior:_getCap(desc)
    return assert(desc.capacity, "Could not find capacity!")
end


local function assertMonstersEqual(lhs, rhs)
    luaunit.assertEquals(lhs.species, rhs.species)
end

local function assertMonsterAt(box, index, expected)
    luaunit.assertNotNil(box)
    local actual = box[index]
    luaunit.assertNotNil(actual)
    assertMonstersEqual(actual, expected)
end

local function assertCanCopyFromTo(srcBox, srcIndex, destBox, destIndex)
    local srcMon = srcBox[srcIndex]
    local newBox = destBox:withMonAt(srcBox[srcIndex], destIndex)
    assertMonsterAt(newBox, destIndex, srcMon)
end

local function assertCopyFromToFails(srcBox, srcIndex, destBox, destIndex)
    luaunit.assertNil(destBox:withMonAt(srcBox[srcIndex], destIndex))
end

local function mapMonstersAreNil(box)
    local nilness = {}
    for i = 1, box.capacity do
       nilness[i] = box[i] == nil
    end
    return nilness
end

local function assertMonstersAreNil(box, nilness)
    luaunit.assertEquals(box.capacity, #nilness)
    for i = 1, box.capacity do
        if nilness[i] then
            luaunit.assertNil(box[i])
        else
            luaunit.assertNotNil(box[i])
        end
    end
end

local function assertCanClear(box, index)
    local shouldBeNil = mapMonstersAreNil(box)
    local modifiedBox = box:withMonAt(nil, index)
    shouldBeNil[index] = true
    assertMonstersAreNil(modifiedBox, shouldBeNil)
end

local function assertClearFails(box, index)
    luaunit.assertNil(box:withMonAt(nil, index))
end

local assertions = {}

function assertions.canCopyIntoSelf(box)
    assertCanCopyFromTo(box, 1, box, 1)
end

function assertions.canCopyIntoExisting(box)
    assertCanCopyFromTo(box, 1, box, 2)
end

function assertions.canCopyIntoFirstEmpty(box, len)
    assertCanCopyFromTo(box, 1, box, len + 1)
end

function assertions.copyingIntoSecondEmptyFails(box, len)
    assertCopyFromToFails(box, 1, box, len + 2)
end

function assertions.copyingIntoInvalidFails(box)
    assertCopyFromToFails(box, 1, box, 0)
    assertCopyFromToFails(box, 1, box, 10000)
    assertCopyFromToFails(box, 1, box, -24)
end

function assertions.canClearEmpty(box, len)
    assertCanClear(box, len + 1)
end

function assertions.canClearLastOccupied(box, len)
    assertCanClear(box, len)
end

function assertions.clearingFirstOccupiedFails(box)
    assertClearFails(box, 1)
end


function BoxInsertingBehavior:getTests(desc, getBox, testName, hist)
    local tests = {}
    local function addTest(name, ...)
        local args = {n = select("#", ...), ...}
        local testName = testName .. "-test" .. name:sub(1, 1):upper() .. name:sub(2)
        tests[testName] = function ()
            assertions[name](getBox(), table.unpack(args))
        end
    end
    local len = self:_getLen(desc)
    local cap = self:_getCap(desc)
    if 1 <= len then addTest("canCopyIntoSelf") end
    if 2 <= len then addTest("canCopyIntoExisting") end
    if 1 <= len and len < cap then addTest("canCopyIntoFirstEmpty", len) end
    if 1 <= len and len < cap - 1 then addTest("copyingIntoSecondEmptyFails", len) end
    if 1 <= len then addTest("copyingIntoInvalidFails") end
    if len < cap then addTest("canClearEmpty", len) end
    if 1 <= len and len <= cap then addTest("canClearLastOccupied", len) end
    if 1 <= len and 3 <= cap then addTest("clearingFirstOccupiedFails") end
    return tests
end

return BoxInsertingBehavior
