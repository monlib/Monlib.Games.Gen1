--[[
    A stack class, which isn't modified by its methods. Nil safety is unknown.
]]

local ImmutableStack = {}
ImmutableStack.__index = ImmutableStack

function ImmutableStack.new(...)
    return setmetatable({ n = select("#", ...), ... }, ImmutableStack) 
end

function ImmutableStack:__len()
    return self.n
end

function ImmutableStack:push(...)
    if self.n >= 1 then
        return ImmutableStack.new(table.unpack(self, 1, self.n), ...)
    else
        return ImmutableStack.new(...)
    end
end

function ImmutableStack:pop()
    assert(self.n >= 1)
    return ImmutableStack.new(table.unpack(self, 1, self.n - 1)), self:peek()
end

function ImmutableStack:peek()
    return self[self.n]
end

function ImmutableStack:at(i)
    return self[i]
end

return ImmutableStack