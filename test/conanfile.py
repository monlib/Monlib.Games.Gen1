import os

from conans import ConanFile, CMake

class MonlibGamesGen1Tests(ConanFile):
    settings = 'os', 'compiler', 'arch', 'build_type'
    generators = 'cmake'
    build_requires = 'cmake_installer/[~3.8.2]@conan/stable'
    requires = 'catch2/2.1.0@bincrafters/stable'

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy('*.dll', dst='bin', src='bin')
        self.copy('*.dylib*', dst='bin', src='lib')
        self.copy('*.so*', dst='bin', src='lib')

    def test(self):
        os.chdir('bin')
        self.run('.{}tests'.format(os.sep))
        self.run('.{}monlibApiCheck'.format(os.sep))
