#include "catch.hpp"

#include "Monlib/Games/Gen1/Interpret/Encodings.h"

using namespace Monlib::Games::Gen1::Interpret;
using Catch::Matchers::Equals;

SCENARIO("English generation 1 strings are converted") {
    GIVEN("The properly terminated english gen1 string 'WORD'") {
        const std::array encoded{
            std::byte{0x96},
            std::byte{0x8E},
            std::byte{0x91},
            std::byte{0x83},
            std::byte{0x50},
        };
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the valid utf-8 string 'WORD'") {
                REQUIRE_THAT(decoded.value(), Equals("WORD"));
            }
        }
    }

    GIVEN("The properly terminated english gen1 string 'WORD' with trailing bytes") {
        const std::array encoded{
            std::byte{0x96},
            std::byte{0x8E},
            std::byte{0x91},
            std::byte{0x83},
            std::byte{0x50},
            std::byte{0x01},
            std::byte{0x79},
            std::byte{0x8A},
        };
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the valid utf-8 string 'WORD' without trailing "
                 "characters") {
                REQUIRE_THAT(decoded.value(), Equals("WORD"));
            }
        }
    }

    GIVEN("The unterminated english gen1 string 'WORD'") {
        const std::array encoded{
            std::byte{0x96}, std::byte{0x8E}, std::byte{0x91}, std::byte{0x83}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the properly terminated valid utf-8 string 'WORD'") {
                REQUIRE_THAT(decoded.value(), Equals("WORD"));
            }
        }
    }

    GIVEN("The english gen1 string 'word'") {
        const std::array encoded{
            std::byte{0xB6}, std::byte{0xAE}, std::byte{0xB1}, std::byte{0xA3}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the utf-8 string 'word'") {
                REQUIRE_THAT(decoded.value(), Equals("word"));
            }
        }
    }

    GIVEN("The english gen1 string '''();[]'-?!▷▶▼♂×/,♀'''") {
        const std::array encoded{
            std::byte{0x9A},
            std::byte{0x9B},
            std::byte{0x9C},
            std::byte{0x9D},
            std::byte{0x9E},
            std::byte{0x9F},
            std::byte{0xE0},
            std::byte{0xE3},
            std::byte{0xE6},
            std::byte{0xE7},
            std::byte{0xEC},
            std::byte{0xED},
            std::byte{0xEE},
            std::byte{0xEF},
            std::byte{0xF1},
            std::byte{0xF3},
            std::byte{0xF4},
            std::byte{0xF5},
        };
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the equivalent valid utf-8 string") {
                REQUIRE_THAT(decoded.value(), Equals("():;[]'-?!▷▶▼♂×/,♀"));
            }
        }
    }

    GIVEN("The english gen1 string '..'") {
        const std::array encoded{std::byte{0xF2}, std::byte{0xE8}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The different meanings are discarded because only 0xF2 is "
                 "accessible.") {
                REQUIRE_THAT(decoded.value(), Equals(".."));
            }
        }
    }

    GIVEN("The english gen1 string '0123456789'") {
        const std::array encoded{
            std::byte{0xF6},
            std::byte{0xF7},
            std::byte{0xF8},
            std::byte{0xF9},
            std::byte{0xFA},
            std::byte{0xFB},
            std::byte{0xFC},
            std::byte{0xFD},
            std::byte{0xFE},
            std::byte{0xFF},
        };
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the utf-8 string '0123456789'") {
                REQUIRE_THAT(decoded.value(), Equals("0123456789"));
            }
        }
    }

    GIVEN("The english gen1 string '▷'") {
        const std::array encoded{std::byte{0xEC}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the utf-8 string '▷'") {
                REQUIRE_THAT(decoded.value(), Equals("▷"));
            }
        }
    }

    GIVEN("An invalid english gen1 string") {
        const std::array encoded{std::byte{0x01}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is a nullopt") { REQUIRE(!decoded); }
        }
    }

    GIVEN("An empty gen1 string") {
        // An empty std::array would return nullptr from data().
        const std::array encoded{std::byte{0x50}};
        const int size{0};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes({encoded.data(), size});
            THEN("The result is the empty string") {
                REQUIRE_THAT(decoded.value(), Equals(""));
            }
        }
    }

    GIVEN("The english gen1 string ' '") {
        const std::array encoded{std::byte{0x7F}};
        WHEN("converted") {
            const auto decoded = Utf8FromEnglishBytes(encoded);
            THEN("The result is the utf-8 string ' '") {
                REQUIRE_THAT(decoded.value(), Equals(" "));
            }
        }
    }

    GIVEN("A length greater than the encoded data's length") {
        WHEN("converted") {
            THEN("Undefined behaviour is encountered.") { SUCCEED(); }
        }
    }
}
