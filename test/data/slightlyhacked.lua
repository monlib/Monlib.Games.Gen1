filename = "data/slightlyhacked.sav"
friendlyname = "slightlyhacked"
local usTeamUUID = "\x0C\x46\x62\xCE\x6D\xF3\x4A\x83\x8C\x6D\xF3\x3F\xAD\x06\xEC\xFA"
local usBoxUUID = "\xCD\x74\x31\x7B\x89\x5B\x4C\xF6\xB0\x7B\x5A\xD9\x3C\x85\x05\xE0"
local usDaycareUUID = "\x4F\xA6\x1F\x0A\xEC\xB7\x45\x96\x8C\x27\x41\x08\x02\x8C\x38\x48"
local usPk1UUID = "\x74\xDD\x0E\xD6\x4D\xA5\x44\xA3\xA1\x99\x70\x2E\x6F\x3D\x09\xFE"
expectedObjects = {
    monsterStorage = {
        [lengthProperty] = 14,
        pcStorage = {
            [lengthProperty] = 12,
            [foreach(1, 12)] = function (i) return sameAs("monsterStorage", i) end,
            [13] = expectNil,
        },
        team = {
            acceptedBinaryFormats = { usTeamUUID, usPk1UUID, },
            capacity = 6,
            [-4] = expectNil,
            [0] = expectNil,
            [1] = {
                binaryFormats = { usTeamUUID, usPk1UUID, },
                species = 16,
                experience = 1059860,
                level = 100,
                originalTrainerName = "RED",
                originalTrainerId = 00981,
                [newBehavior()] = monsterEditing,
                [newBehavior()] = serializationTo(usTeamUUID, "data/serialized/oldbird.bin"),
                [newBehavior()] = serializationTo(usPk1UUID, "data/serialized/oldbird.pk1"),
            },
            [5] = expectNil,
            [6] = expectNil,
            [7] = expectNil,
            [30000] = expectNil,
            [newBehavior()] = boxInserting,
        },
        daycare = {
            acceptedBinaryFormats = { usBoxUUID, },
            capacity = 1,
            [-142] = expectNil,
            [1] = {
                binaryFormats = { usBoxUUID, },
                species = 72,
                [newBehavior()] = serializationTo(usBoxUUID, "data/serialized/jellyfish.bin"),
            },
            [15] = expectNil,
        },
        [foreach(1, 12)] = function (i) return {
            capacity = 20,
        } end,
        [1] = {
            acceptedBinaryFormats = { usBoxUUID, },
            capacity = 20,
            [1] = {
                binaryFormats = { usBoxUUID, },
                nickname = "Cubone",
                species = 104,
                originalTrainerName = "Ash",
                [newBehavior()] = serializationTo(usBoxUUID, "data/serialized/clacking.bin"),
            },
            [5] = {
                level = 91,
            },
            [12] = expectNil,
            [newBehavior()] = boxInserting,
        },
        [12] = { -- Currently active box.
            [1] = {
                species = 19,
            },
        },
        [13] = sameAs("monsterStorage", "team"),
        [14] = sameAs("monsterStorage", "daycare"),
        [15] = expectNil,
        [newBehavior()] = monsterStorageInsertBox,
    },
    [newBehavior()] = saveSerializing(filename),
}
